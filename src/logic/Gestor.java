package logic;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Gestor 
{
	private static String CONNECTION = "jdbc:hsqldb:hsql://localhost/mdb";
	private static String USERNAME = "SA";
	private static String PASSWORD = "";	
	
	private static ArrayList<Producto> productosTotales = new ArrayList<Producto>(); 
	private static ArrayList<Pedido> pedidos = new ArrayList<Pedido>();
	private ArrayList<Producto> productosCarrito = new ArrayList<Producto>();
	
	private static ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	
	private Producto seleccionado = null;
	private Producto seleccionadoCarrito = null;
	
	private double precioProductosIguales;
	
	public Gestor()
	{
		pedidos = new ArrayList<Pedido>();
		productosCarrito = new ArrayList<Producto>();
		
		try 
		{
			rellenarCatalogo();
			rellenarClientes();
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public ArrayList<Producto> rellenarProductosAlmacenero(int idPedido) throws SQLException
	{
		ArrayList<Producto> productosPedido=new ArrayList<Producto>();
		
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM PRODUCTO pr ,COMPRA c WHERE c.idPedido = ? AND c.idProducto = pr.idProducto");
		ps.setInt(1, idPedido);
		ResultSet rs = ps.executeQuery();
		
		while( rs.next())
		{
			int idProducto = rs.getInt("idProducto");
			String nombre = rs.getString("nombre");
			String tipo = rs.getString("subcategoria");
			String descripcion = rs.getString("descripcion");
			String imagen = rs.getString("imagen");
			double precio = rs.getDouble("precio");
			int cantidad = rs.getInt("cantidad");
			String localizacionAlmacen = rs.getString("localizacion");
			int idAlmacen = rs.getInt("idAlmacen");
			Producto producto = new Producto(idProducto, nombre, tipo, descripcion, imagen, precio, cantidad, localizacionAlmacen, idAlmacen);
			productosPedido.add(producto);
		}
		
		return productosPedido;
	}
	
	/**
	 * Se obtienen todos los campos de la tabla Producto de la base de datos y se añaden al array de ProductosTotales
	 * @throws SQLException
	 */
	public static void rellenarCatalogo() throws SQLException
	{
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM PRODUCTO");
		
		while (rs.next()) 
		{
			int idProducto = rs.getInt("idProducto");
			String nombre = rs.getString("nombre");
			String tipo = rs.getString("subcategoria");
			String descripcion = rs.getString("descripcion");
			String imagen = rs.getString("imagen");
			double precio = rs.getDouble("precio");
			int cantidad = rs.getInt("cantidad");
			String localizacionAlmacen = rs.getString("localizacion");
			int idAlmacen = rs.getInt("idAlmacen");
			Producto producto = new Producto(idProducto, nombre, tipo, descripcion, imagen, precio, cantidad, localizacionAlmacen, idAlmacen);
			productosTotales.add(producto);
		}
	}
	
	public static void rellenarClientes() throws SQLException 
	{
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM CLIENTE");
		
		while (rs.next()) 
		{
			int idCliente = rs.getInt("idCliente");
			String usuario = rs.getString("usuario");
			String contraseña = rs.getString("password");
			String nombre = rs.getString("nombre");
			String dni = rs.getString("dni");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");
			String ncuenta = rs.getString("numerocuenta");
			String ntarjeta= rs.getString("numerotarjeta");
			boolean minorista = rs.getBoolean("minorista");

			Cliente cliente = new Cliente(idCliente, usuario, contraseña, nombre, dni, direccion, telefono, ncuenta, ntarjeta, minorista);
			clientes.add(cliente);
		}
	}
	
	/**
	 * Conexión con la base de datos dada por la URL, usuario y contraseña definidos al principio de la clase
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException
	{
		return DriverManager.getConnection(CONNECTION, USERNAME, PASSWORD);
	}
	
	/**
	 * Método que rellenará la tabla con las ordenes de trabajo para el almacenero que se obtendrán
	 * de la tabla pedido
	 * @throws SQLException 
	 */
	public static void rellenarPedidos() throws SQLException 
	{
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM PEDIDO");
		
		while (rs.next()) 
		{
			int idPedido = rs.getInt("idPedido");
			double precioTotal = rs.getDouble("precioTotal");
			String fecha = rs.getString("fecha");
			boolean finalizado = rs.getBoolean("finalizado");
			int idCliente = rs.getInt("idCliente");
			Pedido pedido = new Pedido(idPedido, precioTotal, fecha, finalizado, idCliente);
			pedidos.add(pedido);
		}
	}
	
	public static void rellenarArrayPedidos() throws SQLException 
	{
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM PEDIDO");
		
		while (rs.next()) 
		{
			int idPedido = rs.getInt("idpedido");			
			double precioTotal = rs.getDouble("preciototal");
			String fecha = rs.getString("fecha");
			boolean finalizado = rs.getBoolean("finalizado");
			int idCliente = rs.getInt("idcliente");
			Pedido pedido = new Pedido(idPedido, precioTotal, fecha, finalizado, idCliente);
			pedidos.add(pedido);
			pedido.setProductos(compararPedidos());
		}
	}
	
	public static ArrayList<Producto> compararPedidos() throws SQLException 
	{
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery("SELECT idproducto FROM COMPRA c, PEDIDO p WHERE p.idpedido = c.idpedido");
		
		ArrayList<Producto> arrayProductos = new ArrayList<Producto>();
		
		while (rs.next()) 
		{
			PreparedStatement ps2 = con.prepareStatement("SELECT * FROM PRODUCTO p WHERE p.idproducto = ?");
			int idproducto = rs.getInt("idproducto");
			ps2.setInt(1, idproducto);
			ResultSet rs2 = ps2.executeQuery();
			
			while (rs2.next()) 
			{
				
				int idProducto = rs2.getInt("idProducto");
				String nombre = rs2.getString("nombre");
				String tipo = rs2.getString("subcategoria");
				String descripcion = rs2.getString("descripcion");
				String imagen = rs2.getString("imagen");
				double precio = rs2.getDouble("precio");
				int cantidad = rs2.getInt("cantidad");
				String localizacionAlmacen = rs2.getString("localizacion");
				int idAlmacen = rs2.getInt("idAlmacen");
				Producto producto = new Producto(idProducto, nombre, tipo, descripcion, imagen, precio, cantidad, localizacionAlmacen, idAlmacen);
				arrayProductos.add(producto);
			}
		}
		
		return arrayProductos;
	}
	
	public void añadirCliente(Cliente cliente)
	{
		if(cliente != null)
		{
			clientes.add(cliente);
		}
	}
	
	public void eliminarCliente(Cliente cliente)
	{
		if(cliente != null)
		{
			clientes.remove(cliente);
		}
	}
	
	public boolean comprobarContraseñas(String usuario, String contraseña)
	{
		for(Cliente cliente: clientes)
		{
			if(cliente.getUsuario().equals(usuario) && cliente.getContraseña().equals(contraseña))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void registrarCliente(String usuario, String contraseña, String nombre, String dni, String direccion, String telefono, String nCuenta, String nTarjeta, boolean minorista) throws SQLException
	{
		if(nombre != null && nombre != "" && direccion != null && direccion != "" && telefono != null && telefono != "" && usuario != null && usuario != "" && contraseña != null && contraseña != "" && nCuenta != null && nCuenta != "" && nTarjeta != null && nTarjeta != "")
		{
			Connection con = Gestor.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO CLIENTE VALUES(null, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			ps.setString(1, usuario);
			ps.setString(2, contraseña);
			ps.setString(3, nombre);
			ps.setString(4, dni);
			ps.setString(5, direccion);
			ps.setString(6, telefono);
	        ps.setString(7, nCuenta);
	        ps.setString(8, nTarjeta);
	        ps.setBoolean(9, minorista);
			
			ps.executeUpdate();
			
			rellenarClientes();
		}
	}
	
	public Cliente getClienteActual(String usuario) throws SQLException
	{
		Connection con = Gestor.getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT * FROM CLIENTE WHERE usuario = ?");
		
		ps.setString(1, usuario);
		ResultSet rs = ps.executeQuery();
		
		Cliente cliente = null;
		
		while(rs.next())
		{
			int idCliente = rs.getInt("idCliente");
			String usuarioActual = rs.getString("usuario");
			String contraseña = rs.getString("password");
			String nombre = rs.getString("nombre");
			String dni = rs.getString("dni");
			String direccion = rs.getString("direccion");
			String telefono = rs.getString("telefono");
			String ncuenta = rs.getString("numerocuenta");
			String ntarjeta = rs.getString("numeroTarjeta");
			boolean minorista = rs.getBoolean("minorista");
			
			cliente = new Cliente(idCliente, usuarioActual, contraseña, nombre, dni, direccion, telefono, ncuenta, ntarjeta, minorista);
		}
		
		return cliente;
	}
	
//	public void registrarMinorista(int idMinorista, String nombre, String dni, String direccion, String telefono, String usuario, String contraseña)
//	{
//		if(nombre != null && nombre != "" && direccion != null && direccion != "" && telefono != null && telefono != "" && usuario != null && usuario != "" && contraseña != null && contraseña != "")
//		{
//			minoristas.add(new Minorista(idMinorista, nombre, dni, direccion, telefono, usuario, contraseña));
//		}
//	}
	
	public void añadirPedido(int idCliente)
	{
		double precioTotal = 0;
		for (Producto p: productosCarrito)
		{
			precioTotal = precioTotal + p.getPrecio();
		}
		
		Date fechaActual = new Date();
		DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = formatoFecha.format(fechaActual);
		
		
		Pedido pedido = new Pedido(0,precioTotal, fecha, false, idCliente);
		pedidos.add(pedido);
	}
	
	public void seleccionarProducto(int pos)
	{
		seleccionado = productosTotales.get(pos);
	}
	
	public void seleccionarProductoCarrito(int pos)
	{
		seleccionadoCarrito = productosCarrito.get(pos);
	}
	
	public Producto getProductoSeleccionado()
	{
		return seleccionado;
	}
	
	public Producto getProductoSeleccionadoCarrito()
	{
		return seleccionadoCarrito;
	}
	
	public void añadirProductoCarrito(Producto producto, int unidades)
	{
		for(int i = 0; i < unidades; i ++)
		{
			productosCarrito.add(producto);
		}
		
		precioCarrito();
	}
	
	public void eliminarProductoCarrito(Producto producto)
	{
		productosCarrito.remove(producto);
		precioCarrito();
	}
	
	public double precioCarrito()
	{
		double precioCarrito = 0;
		
		for(Producto p: productosCarrito)
		{
			precioCarrito += p.getPrecio();
		}
		
		return redondearDecimales(precioCarrito, 2);
	}
	
	public double redondearDecimales(double valorInicial, int numeroDecimales)
	{
        double parteEntera, resultado;
        resultado = valorInicial;
        
        parteEntera = Math.floor(resultado);
        resultado =(resultado-parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado =(resultado/Math.pow(10, numeroDecimales)) + parteEntera;
        
        return resultado;
    }
	
	public double calcularPrecioMismoProducto(Producto producto, int cantidad)
	{
		double precioProductosIguales = 0;
		
		for(Producto p: productosCarrito)
		{
			if(p.getIdProducto() == producto.getIdProducto())
			{
				precioProductosIguales = p.getPrecio() * cantidad;
			}
		}
		
		return precioProductosIguales;
	}
	
	public void setPrecioProductosIguales(int precioProductosIguales)
	{
		this.precioProductosIguales = precioProductosIguales;
	}
	
	public double getPrecioMismoProducto()
	{
		return precioProductosIguales;
	}
	
	public ArrayList<Cliente> getArrayClientes()
	{
		return clientes;
	}
	
	public void eliminarPedido(Pedido pedido)
	{
		pedidos.remove(pedido);
	}
	
	public static ArrayList<Producto> getProductosTotales()
	{
		return productosTotales;
	}
	
	public ArrayList<Producto> getProductosCarrito()
	{
		return productosCarrito;
	}
	
	public void vaciarProductosCarrito()
	{
		productosCarrito.clear();
		precioCarrito();
	}
	
	public ArrayList<Pedido> getPedidos() 
	{
		return pedidos;
	}

	public void setPedidos(ArrayList<Pedido> pedidos) 
	{
		this.pedidos = pedidos;
	}
	
	public String rellenarInforme(Pedido pedido){
		
		StringBuilder sb=new StringBuilder();
		sb.append("Id del Pedido:                        ");
		sb.append(pedido.getIdPedido());
		sb.append("\nPedido el día:                    ");
		sb.append(pedido.getFecha());
		sb.append("\nId del Cliente:                    ");
		sb.append(pedido.getIdCliente());
		sb.append("\n\nLista Productos:");
		sb.append("\n\n  Producto                                    Precio ");
		
		for(Producto producto: pedido.getProductos()){
			sb.append("\n "+producto.getNombre());
			sb.append("                         "+producto.getPrecio()+"€");
		}
		
		sb.append("\n\nPrecio Total:                              ");
		sb.append(pedido.getPrecioTotal()+"€");
		
		return sb.toString();
	}

	public void EncargarPedido(Pedido pedido)
	{
		for(Producto producto: pedido.getProductos())
		{
			producto.setCantidad(producto.getCantidad()-1);
		}
		
		eliminarPedido(pedido);
	}
	
	public boolean isProductoValidado(int idProducto, int idPedido) throws SQLException
	{
		boolean validado= false;
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT VALIDADO FROM COMPRA c WHERE c.idPedido = ? AND c.idProducto = ?");
		ps.setInt(1, idPedido);
		ps.setInt(2, idProducto);
		ResultSet rs = ps.executeQuery();
		
		while( rs.next())
		{
			validado=rs.getBoolean("VALIDADO");
		}
		
		return validado;	
	}
	
	public boolean isProductoEmpaquetado(int idProducto, int idPedido) throws SQLException
	{
		boolean validado= false;
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement("SELECT EMPAQUETADO FROM COMPRA c WHERE c.idPedido = ? AND c.idProducto = ?");
		ps.setInt(1, idPedido);
		ps.setInt(2, idProducto);
		ResultSet rs = ps.executeQuery();
		while( rs.next())
		{
			validado=rs.getBoolean("EMPAQUETADO");
		}
		
		return validado;
	}
	
	public void finalizarPedido(int idPedido) throws SQLException
	{
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement("UPDATE PEDIDO p set FINALIZADO = true WHERE p.idPedido = ?");
		ps.setInt(1, idPedido);
		ps.executeUpdate();
	}
	
	public void validarProducto(int idProducto, int idPedido) throws SQLException
	{
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement("UPDATE COMPRA c set VALIDADO = true WHERE c.idPedido = ? AND c.idProducto = ?");
		ps.setInt(1, idPedido);
		ps.setInt(2, idProducto);
		ps.executeUpdate();
	}
	
	public void empaquetarProducto(int idProducto, int idPedido) throws SQLException
	{
		Connection con = getConnection();
		PreparedStatement ps= con.prepareStatement("UPDATE COMPRA c set EMPAQUETADO = true WHERE c.idPedido = ? AND c.idProducto = ?");
		ps.setInt(1, idPedido);
		ps.setInt(2, idProducto);
		ps.executeUpdate();
	}
}