package logic;

public class Minorista 
{
	private int idMinorista;
	private String nombre;
	private String dni;
	private String direccion;
	private String telefono;
	
	private String usuario;
	private String contraseña;
	
	public Minorista(int idMinorista, String nombre, String dni, String direccion, String telefono, String usuario, String contraseña) 
	{
		this.idMinorista = idMinorista;
		this.nombre = nombre;
		this.dni = dni;
		this.direccion = direccion;
		this.telefono = telefono;
		
		this.usuario = usuario;
		this.contraseña = contraseña;
	}
	
	public int getIdMinorista() 
	{
		return idMinorista;
	}
	public void setIdMinorista(int idMinorista)
	{
		this.idMinorista = idMinorista;
	}
	public String getNombre() 
	{
		return nombre;
	}
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	public String getDni() 
	{
		return dni;
	}
	public void setDni(String dni)
	{
		this.dni = dni;
	}
	public String getDireccion() 
	{
		return direccion;
	}
	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}
	public String getTelefono() 
	{
		return telefono;
	}
	public void setTelefono(String telefono)
	{
		this.telefono = telefono;
	}

	public String getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(String usuario) 
	{
		this.usuario = usuario;
	}

	public String getContraseña() 
	{
		return contraseña;
	}

	public void setContraseña(String contraseña)
	{
		this.contraseña = contraseña;
	}
}