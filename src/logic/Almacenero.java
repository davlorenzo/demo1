package logic;

public class Almacenero 
{
	private int idAlmacenero;
	private String nombre;
	private int edad;
	private String direccion;
	
	private int idAlmacen;
	
	public Almacenero(int idAlmacenero, String nombre, int edad, String direccion, int idAlmacen) 
	{
		this.idAlmacenero = idAlmacenero;
		this.nombre = nombre;
		this.edad = edad;
		this.direccion = direccion;
		this.idAlmacen = idAlmacen;
	}

	public int getIdAlmacenero() 
	{
		return idAlmacenero;
	}

	public void setIdAlmacenero(int idAlmacenero) 
	{
		this.idAlmacenero = idAlmacenero;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public int getEdad() 
	{
		return edad;
	}

	public void setEdad(int edad) 
	{
		this.edad = edad;
	}

	public String getDireccion() 
	{
		return direccion;
	}

	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}

	public int getIdAlmacen() 
	{
		return idAlmacen;
	}

	public void setIdAlmacen(int idAlmacen) 
	{
		this.idAlmacen = idAlmacen;
	}
}