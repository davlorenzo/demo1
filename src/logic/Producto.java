package logic;

public class Producto {

	private int idProducto;
	private String nombre;
	private String tipo;
	private String descripcion;
	private String imagen;
	private double precio;
	private int cantidad;
	private String localizacionAlmacen;
	private int idAlmacen;
	
	private boolean validado;
	private boolean empaquetado;
	
	public Producto(int idProducto, String nombre, String tipo, String descripcion,
			String imagen, double precio, int cantidad,
			String localizacionAlmacen, int idAlmacen) {
		
		validado=false;
		empaquetado=false;
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.tipo = tipo;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.precio = precio;
		this.cantidad = cantidad;
		this.localizacionAlmacen = localizacionAlmacen;
		this.idAlmacen = idAlmacen;		
	}
	
	
	
	public boolean isValidado() {
		return validado;
	}



	public void setValidado(boolean validado) {
		validado = validado;
	}



	public boolean isEmpaquetado() {
		return empaquetado;
	}



	public void setEmpaquetado(boolean empaquetado) {
		empaquetado = empaquetado;
	}



	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getLocalizacionAlmacen() {
		return localizacionAlmacen;
	}
	public void setLocalizacionAlmacen(String localizacionAlmacen) {
		this.localizacionAlmacen = localizacionAlmacen;
	}
	public int getIdAlmacen() {
		return idAlmacen;
	}
	public void setIdAlmacen(int idAlmacen) {
		this.idAlmacen = idAlmacen;
	}
	
}
