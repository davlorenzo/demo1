package logic;

import java.util.ArrayList;

public class Pedido {
	
	private int idPedido;

	private double precioTotal;
	private String fecha;
	private boolean finalizado;
	private int idCliente;
	private ArrayList<Producto> productos;
	
	public Pedido(int idPedido, double precioTotal, String fecha, boolean finalizado,
			int idCliente) 
	{
		this.idPedido = idPedido;
		this.precioTotal = precioTotal;
		this.fecha = fecha;
		this.finalizado = finalizado;
		this.idCliente = idCliente;
	}
	
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public double getPrecioTotal() {
		return precioTotal;
	}
	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public boolean getFinalizado() {
		return finalizado;
	}
	public void setfinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}


	public ArrayList<Producto> getProductos() {
		return productos;
	}


	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

}
