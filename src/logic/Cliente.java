package logic;

public class Cliente 
{
	private int idCliente;
	
	private String usuario;
	private String contraseña;
	
	private String nombre;
	private String dni;
	private String direccion;
	private String telefono;
	
	private String nCuenta;
	private String nTarjeta;
	
	private boolean minorista;
	
	public Cliente(int idCliente, String usuario, String contraseña, String nombre, String dni, String direccion, String telefono, String nCuenta, String nTarjeta, boolean minorista) 
	{
		this.idCliente = idCliente;
		
		this.usuario = usuario;
		this.contraseña = contraseña;
		
		this.nombre = nombre;
		this.dni = dni;
		this.direccion = direccion;
		this.telefono = telefono;
		
		this.nCuenta = nCuenta;
		this.nTarjeta = nTarjeta;

		this.minorista = minorista;
	}
	
	public boolean isMinorista() 
	{
		return minorista;
	}

	public void setMinorista(boolean minorista) 
	{
		this.minorista = minorista;
	}

	public String getNCuenta() 
	{
		return nCuenta;
	}

	public void setNCuenta(String nCuenta) 
	{
		this.nCuenta = nCuenta;
	}

	public String getNTarjeta() 
	{
		return nTarjeta;
	}

	public void setNTarjeta(String nTarjeta)
	{
		this.nTarjeta = nTarjeta;
	}

	public int getIdCliente() 
	{
		return idCliente;
	}
	public void setIdCliente(int idCliente)
	{
		this.idCliente = idCliente;
	}
	public String getNombre() 
	{
		return nombre;
	}
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	public String getDni() 
	{
		return dni;
	}
	public void setDni(String dni) 
	{
		this.dni = dni;
	}
	public String getDireccion() 
	{
		return direccion;
	}
	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}
	public String getTelefono() 
	{
		return telefono;
	}
	public void setTelefono(String telefono)
	{
		this.telefono = telefono;
	}

	public String getUsuario() 
	{
		return usuario;
	}

	public void setUsuario(String usuario) 
	{
		this.usuario = usuario;
	}

	public String getContraseña() 
	{
		return contraseña;
	}

	public void setContraseña(String contraseña)
	{
		this.contraseña = contraseña;
	}
}