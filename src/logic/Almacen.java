package logic;

public class Almacen {
	
	private int idAlmacen;
	private String nombre;
	private String ciudad;
	private int idProveedor;
	
	
	
	
	public Almacen(int idAlmacen, String nombre, String ciudad, int idProveedor) {
		
		this.idAlmacen = idAlmacen;
		this.nombre = nombre;
		this.ciudad = ciudad;
		this.idProveedor = idProveedor;
	}
	public int getIdAlmacen() {
		return idAlmacen;
	}
	public void setIdAlmacen(int idAlmacen) {
		this.idAlmacen = idAlmacen;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public int getIdProveedor() {
		return idProveedor;
	}
	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}
	
	
}
