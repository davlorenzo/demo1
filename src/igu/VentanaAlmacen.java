package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.CardLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import logic.Gestor;
import logic.Pedido;
import logic.Producto;


import javax.swing.border.LineBorder;
import javax.swing.ListSelectionModel;
import java.awt.Panel;

@SuppressWarnings("serial")
public class VentanaAlmacen extends JFrame
{
	private JPanel contentPane;
	private JPanel pnCard;
	private JPanel pnCard_0;
	private JPanel panel_9;
	private JPanel panel_11;
	private JScrollPane scrollPane;
	private JTable tablaPedidos;
	private JPanel panel_26;
	private JPanel panel_27;
	private JScrollPane scrollPane_1;
	private JPanel panel_31;
	private JButton btnValidarPedido;
	private JPanel pnCard_4;
	private JPanel btConfiPedido;
	private JPanel pnTexto;
	private JButton btnAtrs;
	private JButton btnConfirmarPedido;
	private JPanel panel_1;
	private JLabel lblResumenDel;
	private JScrollPane scrollPane_4;
	private JTextArea txtResumen;
	private JPanel pnCard_1;
	private JPanel panel;
	private JPanel panel_2;
	private JPanel panel_3;
	private JButton btnValidarProducto;
	private JPanel panel_5;
	private JPanel panel_6;
	private JScrollPane scrollPane_2;
	private JTable tablaValidado;
	private JPanel pnCard_2;
	private Panel panel_7;
	private Panel panel_8;
	private JPanel panel_13;
	private Panel panel_14;
	private JScrollPane scrollPane_3;
	private JTable tablaEmpaquetado;
	private JButton btnFactura;
	private JButton btnAnotarIncidencia;
	private JButton btnAtras;
	private JButton btnEmpaquetar;
	private JButton btnEmpaquetarProductosPedido;
	private JButton btnEmpaquetarProducto;
	private JButton btnAtras_1;
	
	private DefaultTableModel modeloTablePedidos;
	private DefaultTableModel modeloTableValidado;
	private DefaultTableModel modeloTableEmpaquetado;
	
	Gestor gestor;
	
	private Pedido pedidoSeleccionado;
	private Producto productoValidando;
	private Producto productoEmpaquetando;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				
				
				try {
					VentanaAlmacen frame = new VentanaAlmacen();
					
				
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					
				}
			}
		});
	}
	

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaAlmacen() throws SQLException {
		
		
		gestor=new Gestor();
		Gestor.rellenarArrayPedidos();
		try {
			Gestor.rellenarCatalogo();
		} catch (SQLException e) {

			e.printStackTrace();
		}
				
		setBackground(Color.WHITE);
		//setIconImage(Toolkit.getDefaultToolkit().getImage(VentanaPrincipal.class.getResource("/img/Logo.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 391, 518);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPnCard(), BorderLayout.CENTER);
		rellenarTablaPedidos();
		
	}
	
	private JPanel getPnCard() {
		if (pnCard == null) {
			pnCard = new JPanel();
			pnCard.setLayout(new CardLayout(0, 0));
			pnCard.add(getPnCard_0(), "panel0");
			pnCard.add(getPnCard_1(), "panel1");
			pnCard.add(getPnCard_2(), "panel2");
			pnCard.add(getPnCard_4(), "panel3");
			
		}
		return pnCard;
	}
	private JPanel getPnCard_0() {
		if (pnCard_0 == null) {
			pnCard_0 = new JPanel();
			pnCard_0.setBackground(Color.WHITE);
			pnCard_0.setLayout(new BorderLayout(0, 0));
			pnCard_0.add(getPanel_9(), BorderLayout.SOUTH);
			pnCard_0.add(getPanel_11(), BorderLayout.CENTER);
		}
		return pnCard_0;
	}

	private JPanel getPanel_9() {
		if (panel_9 == null) {
			panel_9 = new JPanel();
			panel_9.setBackground(Color.WHITE);
			panel_9.setLayout(new BorderLayout(0, 0));
			panel_9.add(getPanel_31());
		}
		return panel_9;
	}
	private JPanel getPanel_11() {
		if (panel_11 == null) {
			panel_11 = new JPanel();
			panel_11.setBackground(Color.WHITE);
			panel_11.setLayout(new BorderLayout(0, 0));
			panel_11.add(getScrollPane(), BorderLayout.CENTER);
			panel_11.add(getPanel_26(), BorderLayout.SOUTH);
		}
		return panel_11;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTablaPedidos());
			scrollPane.setBackground(Color.WHITE);
		}
		return scrollPane;
	}
	
	private JTable getTablaPedidos() {
		if (tablaPedidos == null) {
			tablaPedidos = new JTable();
			tablaPedidos.setIgnoreRepaint(false);
			tablaPedidos.setSelectionForeground(Color.WHITE);
			tablaPedidos.setGridColor(Color.BLACK);
			tablaPedidos.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			tablaPedidos.setSelectionBackground(Color.WHITE);
			tablaPedidos.setBackground(Color.WHITE);
			tablaPedidos.setBorder(new LineBorder(new Color(0, 0, 255), 3));
			tablaPedidos.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			tablaPedidos.setAutoCreateRowSorter(true);
			tablaPedidos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tablaPedidos.setRowSelectionAllowed(false);
			tablaPedidos.setEnabled(false);
			
			String[] nombresCol= {"Id Pedido","Tamaño"};
			modeloTablePedidos=new ModeloNoEditable(nombresCol,0);
			tablaPedidos = new JTable(modeloTablePedidos);
			tablaPedidos.addMouseListener(new MouseAdapter() {
	
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
				int fila= tablaPedidos.getSelectedRow();
				
				//Comprobación Pedido Disponible
				for (Pedido pedido : gestor.getPedidos()) {
					if(pedido.getIdPedido()==Integer.parseInt((String) tablaPedidos.getValueAt(fila, 0))){
						pedidoSeleccionado=pedido;	
						
						if(comprobarTodosValidados()){
							btnEmpaquetarProductosPedido.setEnabled(true);
							btnValidarPedido.setEnabled(false);
						}
						else{
							btnEmpaquetarProductosPedido.setEnabled(false);
							btnValidarPedido.setEnabled(true);
						}
					}
				}				
			}
			
			
			});
			tablaPedidos.getTableHeader().setReorderingAllowed(false);
		}
		return tablaPedidos;
	}
	
	private JTable getTablaValidado() {
		if (tablaValidado == null) {
			tablaValidado = new JTable();
			tablaValidado.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			
			tablaValidado.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent arg0) {
					
					
					int fila= tablaValidado.getSelectedRow();
					
					boolean productoValidado=false;
					
					try {
						for (Producto producto : gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido())) {
							if(producto.getIdProducto()==Integer.parseInt((String) tablaValidado.getValueAt(fila, 0))){
								
								productoValidando = producto;	
								if(gestor.isProductoValidado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido())){
									productoValidado=true;
								}
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}

					if(!productoValidado){
						
						btnValidarProducto.setEnabled(true);
						btnAnotarIncidencia.setEnabled(true);
						
					}
					else{
						btnValidarProducto.setEnabled(false);
						btnAnotarIncidencia.setEnabled(false);
					}
					
					
					

					
				}
				
				
				});
				tablaPedidos.getTableHeader().setReorderingAllowed(false);
		}
		return tablaValidado;
	}
	
	
	private JTable getTablaEmpaquetado() {
		if (tablaEmpaquetado == null) {
			tablaEmpaquetado = new JTable();
			tablaEmpaquetado.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent arg0) {
					
					
					int fila= tablaEmpaquetado.getSelectedRow();
					
					boolean productoEmpaquetado=false;
					
					try {
						for (Producto producto : gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido())) {
							if(producto.getIdProducto()==Integer.parseInt((String) tablaEmpaquetado.getValueAt(fila, 0))){
								
								productoEmpaquetando = producto;	
								if(gestor.isProductoEmpaquetado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido())){
									
									productoEmpaquetado=true;
								}
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (SQLException e) {
						e.printStackTrace();
					}

					if(!productoEmpaquetado){
						
						btnEmpaquetarProducto.setEnabled(true);
						
					}
					else{
						btnEmpaquetarProducto.setEnabled(false);
					}

					
				}
				
				
				});
			tablaEmpaquetado.getTableHeader().setReorderingAllowed(false);
			
		}
		return tablaEmpaquetado;
	}
	
	
	
	private void rellenarTablaValidados() throws SQLException{
		
		String[] nombresCol= {"Id Producto","Localización Almacen","Validado",};
		modeloTableValidado=new ModeloNoEditable(nombresCol,0);
				
		String[] fila=new String[3];
		
		ArrayList<Producto> productos = new ArrayList<Producto>();
		try {
			productos = gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido());
		} catch (SQLException e) {
			System.out.println("Error de carga de tabla");
			e.printStackTrace();
		}
	
		for(Producto producto: productos){
			fila[0]=String.valueOf(producto.getIdProducto());
			fila[1]=producto.getLocalizacionAlmacen();
			boolean isValidado=false;
			isValidado=gestor.isProductoValidado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido());
			if(isValidado){
				fila[2]="Si";
			}
			else{
				fila[2]="No";
			}
			modeloTableValidado.addRow(fila);
		}
		tablaValidado.getTableHeader().setReorderingAllowed(false);
		tablaValidado.setModel(modeloTableValidado);
		
		
	}
	
	
	private void rellenarTablaPedidos() {
		
		
		String[] fila=new String[3];
		ArrayList<Pedido> pedidos=gestor.getPedidos();
		if(pedidos.isEmpty()){
			
		}
		else{
			for(Pedido pedido: pedidos){
				
				 	ArrayList<Producto> productos = new ArrayList<Producto>();
					try {
						productos = gestor.rellenarProductosAlmacenero(pedido.getIdPedido());
					} catch (SQLException e) {
						System.out.println("Error de carga de tabla");
						e.printStackTrace();
					}
				
					fila[0]=String.valueOf(pedido.getIdPedido());
					fila[1]=String.valueOf(productos.size());
					modeloTablePedidos.addRow(fila);
				
			}
			
			}

		
		tablaPedidos.setSelectionForeground(Color.WHITE);
		tablaPedidos.setGridColor(Color.BLACK);
		tablaPedidos.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
		tablaPedidos.setSelectionBackground(Color.WHITE);
		tablaPedidos.setBackground(Color.WHITE);
		tablaPedidos.setRowSelectionAllowed(false);
	}
	
	
	
	
	public void reiniciarTablaPedidos(){
		int tamañoTabla = modeloTablePedidos.getRowCount();
		for (int i = 0; i < tamañoTabla; i++) {
			modeloTablePedidos.removeRow(i);
			tamañoTabla--;
			i--;
		}
		rellenarTablaPedidos();	
	}
	
	
	public void vaciarTablaValdidado(){
		int tamañoTabla = tablaValidado.getRowCount();
		
		for (int i = 0; i < tamañoTabla; i++) {
			modeloTableValidado.removeRow(i);
			tamañoTabla--;
			i--;
		}
	}
	public void reiniciarTablaValidado(){
		vaciarTablaValdidado();
		try {
			rellenarTablaValidados();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public void reiniciarTablaEmpaquetado(){
		vaciarTablaEmpaquetado();
		try {
			rellenarTablaEmpaquetado();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	
	public void vaciarTablaEmpaquetado(){
	int tamañoTabla = modeloTableEmpaquetado.getRowCount();
		
		for (int i = 0; i < tamañoTabla; i++) {
			modeloTableEmpaquetado.removeRow(i);
			tamañoTabla--;
			i--;
		}
	}
	
	public void rellenarTablaEmpaquetado() throws SQLException{
		
		String[] nombresCol= {"Id Producto","Empaquetado",};
		modeloTableEmpaquetado=new ModeloNoEditable(nombresCol,0);
				
		String[] fila=new String[2];
		
		ArrayList<Producto> productos = new ArrayList<Producto>();
		try {
			productos = gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido());
		} catch (SQLException e) {
			System.out.println("Error de carga de tabla");
			e.printStackTrace();
		}
	
		for(Producto producto: productos){
			fila[0]=String.valueOf(producto.getIdProducto());
			if(gestor.isProductoEmpaquetado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido())){//
				fila[1]="Si";
			}
			else{
				fila[1]="No";
			}
			modeloTableEmpaquetado.addRow(fila);
		}
		tablaEmpaquetado.getTableHeader().setReorderingAllowed(false);
		tablaEmpaquetado.setModel(modeloTableEmpaquetado);
	}
	
	
	
	private JPanel getPanel_26() {
		if (panel_26 == null) {
			panel_26 = new JPanel();
			panel_26.setBackground(Color.WHITE);
			panel_26.setLayout(new BorderLayout(0, 0));
			panel_26.add(getPanel_27(), BorderLayout.CENTER);
		}
		return panel_26;
	}
	private JPanel getPanel_27() {
		if (panel_27 == null) {
			panel_27 = new JPanel();
			panel_27.setBackground(Color.WHITE);
			panel_27.setLayout(new BorderLayout(0, 0));
			panel_27.add(getScrollPane_1(), BorderLayout.CENTER);
		}
		return panel_27;
	}
	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
		}
		return scrollPane_1;
	}
	private JPanel getPanel_31() {
		if (panel_31 == null) {
			panel_31 = new JPanel();
			panel_31.setBackground(Color.WHITE);
			FlowLayout flowLayout = (FlowLayout) panel_31.getLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			panel_31.add(getBtnEmpaquetarProductosPedido());
			panel_31.add(getBtnValidarPedido());

		}
		return panel_31;
	}
	private JButton getBtnValidarPedido() {
		if (btnValidarPedido == null) {
			btnValidarPedido = new JButton("Validar Pedido  ");
			btnValidarPedido.setBackground(Color.WHITE);
			btnValidarPedido.setOpaque(false);
			btnValidarPedido.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnValidarPedido.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnValidarPedido.setEnabled(false);
			btnValidarPedido.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					if(comprobarTodosValidados()){
						btnEmpaquetar.setEnabled(true);
					}
					try {
						rellenarTablaValidados();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					reiniciarPn0();
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel1");
				}
			});
		}
		return btnValidarPedido;
	}
	
	public String generarDoc(){
		
		String texto="";
		return texto;
	}
		
	
	
	public void reiniciar(){
		
		reiniciarPn0();
		reiniciarPn1();
		reiniciarPn2();
		reiniciarPn3();
	
		((CardLayout)pnCard.getLayout()).show(pnCard,"panel0");
		
		
	}
	public void reiniciarPn0(){
	
		btnEmpaquetarProductosPedido.setEnabled(false);
		btnValidarPedido.setEnabled(false);
		reiniciarTablaPedidos();
		
			
	}
	public void reiniciarPn1(){
		reiniciarTablaValidado();
		btnAnotarIncidencia.setEnabled(false);
		btnValidarPedido.setEnabled(false);
		btnEmpaquetar.setEnabled(false);
	}
	
	public void reiniciarPn2(){
		btnEmpaquetarProducto.setEnabled(false);
		btnFactura.setEnabled(false);
		reiniciarTablaEmpaquetado();
	}
	
	public void reiniciarPn3(){
		txtResumen.setText("");
	}
	
	
	
	private JPanel getPnCard_4() {
		if (pnCard_4 == null) {
			pnCard_4 = new JPanel();
			pnCard_4.setLayout(new BorderLayout(0, 0));
			pnCard_4.add(getPanel_1(), BorderLayout.NORTH);
			pnCard_4.add(getBtConfiPedido(), BorderLayout.SOUTH);
			pnCard_4.add(getPnTexto(), BorderLayout.CENTER);
		}
		return pnCard_4;
	}
	private JPanel getBtConfiPedido() {
		if (btConfiPedido == null) {
			btConfiPedido = new JPanel();
			FlowLayout fl_btConfiPedido = (FlowLayout) btConfiPedido.getLayout();
			fl_btConfiPedido.setAlignment(FlowLayout.RIGHT);
			btConfiPedido.setBackground(Color.WHITE);
			btConfiPedido.add(getBtnAtrs());
			btConfiPedido.add(getBtnConfirmarPedido());
		}
		return btConfiPedido;
	}
	private JPanel getPnTexto() {
		if (pnTexto == null) {
			pnTexto = new JPanel();
			pnTexto.setLayout(new BorderLayout(0, 0));
			pnTexto.add(getScrollPane_4(), BorderLayout.NORTH);
			pnTexto.add(getTxtResumen(), BorderLayout.CENTER);
		}
		return pnTexto;
	}
	private JButton getBtnAtrs() {
		if (btnAtrs == null) {
			btnAtrs = new JButton("     Atr\u00E1s     ");
			btnAtrs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel2");
					reiniciarPn3();
				}
			});
			btnAtrs.setOpaque(false);
			btnAtrs.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnAtrs.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnAtrs.setBackground(Color.WHITE);
		}
		return btnAtrs;
	}
	private JButton getBtnConfirmarPedido() {
		if (btnConfirmarPedido == null) {
			btnConfirmarPedido = new JButton("     Confirmar Pedido     ");
			btnConfirmarPedido.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					gestor.EncargarPedido(pedidoSeleccionado);
					
					
					try {
						gestor.finalizarPedido(pedidoSeleccionado.getIdPedido());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
					reiniciar();
				}
			});
			btnConfirmarPedido.setOpaque(false);
			btnConfirmarPedido.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnConfirmarPedido.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnConfirmarPedido.setBackground(Color.WHITE);
		}
		return btnConfirmarPedido;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBackground(Color.WHITE);
			FlowLayout flowLayout = (FlowLayout) panel_1.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			panel_1.add(getLblResumenDel());
		}
		return panel_1;
	}
	private JLabel getLblResumenDel() {
		if (lblResumenDel == null) {
			lblResumenDel = new JLabel("Resumen del pedido:");
			lblResumenDel.setBackground(Color.WHITE);
			lblResumenDel.setFont(new Font("Book Antiqua", Font.PLAIN, 14));
		}
		return lblResumenDel;
	}
	private JScrollPane getScrollPane_4() {
		if (scrollPane_4 == null) {
			scrollPane_4 = new JScrollPane();
		}
		return scrollPane_4;
	}
	private JTextArea getTxtResumen() {
		if (txtResumen == null) {
			txtResumen = new JTextArea();
			txtResumen.setEditable(false);
			txtResumen.setBorder(new LineBorder(new Color(0, 0, 0), 2));
			txtResumen.setBackground(Color.LIGHT_GRAY);
		}
		return txtResumen;
	}
	private JPanel getPnCard_1() {
		if (pnCard_1 == null) {
			pnCard_1 = new JPanel();
			pnCard_1.setLayout(new BorderLayout(0, 0));
			pnCard_1.add(getPanel(), BorderLayout.SOUTH);
			pnCard_1.add(getPanel_2(), BorderLayout.CENTER);
		}
		return pnCard_1;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanel_3(), BorderLayout.NORTH);
		}
		return panel;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setLayout(new BorderLayout(0, 0));
			panel_2.add(getPanel_5_1(), BorderLayout.SOUTH);
			panel_2.add(getPanel_6(), BorderLayout.CENTER);
		}
		return panel_2;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setBackground(Color.WHITE);
			panel_3.add(getBtnAtras());
			panel_3.add(getBtnEmpaquetar());
			panel_3.add(getBtnAnotarIncidencia());
			panel_3.add(getBtnValidarProducto());
		}
		return panel_3;
	}
	private JButton getBtnValidarProducto() {
		if (btnValidarProducto == null) {
			btnValidarProducto = new JButton("Validar");
			btnValidarProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					try {
						gestor.validarProducto(productoValidando.getIdProducto(),pedidoSeleccionado.getIdPedido());
					} catch (SQLException e) {
						e.printStackTrace();
					}
				
					if(comprobarTodosValidados()){
						btnEmpaquetar.setEnabled(true);
					}
					
					reiniciarPn1();
					if(comprobarTodosValidados()){
						btnEmpaquetar.setEnabled(true);
					}
					
				}
			});
			btnValidarProducto.setHorizontalAlignment(SwingConstants.RIGHT);
			btnValidarProducto.setOpaque(false);
			btnValidarProducto.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnValidarProducto.setEnabled(false);
			btnValidarProducto.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnValidarProducto.setBackground(Color.WHITE);
		}
		return btnValidarProducto;
	}
	private JPanel getPanel_5_1() {
		if (panel_5 == null) {
			panel_5 = new JPanel();
			panel_5.setLayout(new BorderLayout(0, 0));
		}
		return panel_5;
	}
	private JPanel getPanel_6() {
		if (panel_6 == null) {
			panel_6 = new JPanel();
			panel_6.setLayout(new BorderLayout(0, 0));
			panel_6.add(getScrollPane_2());
		}
		return panel_6;
	}
	private JScrollPane getScrollPane_2() {
		if (scrollPane_2 == null) {
			scrollPane_2 = new JScrollPane();
			scrollPane_2.setBackground(Color.WHITE);
			scrollPane_2.setViewportView(getTablaValidado());
		}
		return scrollPane_2;
	}
	
	
	private JPanel getPnCard_2() {
		if (pnCard_2 == null) {
			pnCard_2 = new JPanel();
			pnCard_2.setLayout(new BorderLayout(0, 0));
			pnCard_2.add(getPanel_7(), BorderLayout.SOUTH);
			pnCard_2.add(getPanel_8(), BorderLayout.CENTER);
		}
		return pnCard_2;
	}
	private Panel getPanel_7() {
		if (panel_7 == null) {
			panel_7 = new Panel();
			panel_7.setLayout(new BorderLayout(0, 0));
		}
		return panel_7;
	}
	private Panel getPanel_8() {
		if (panel_8 == null) {
			panel_8 = new Panel();
			panel_8.setLayout(new BorderLayout(0, 0));
			panel_8.add(getPanel_13(), BorderLayout.SOUTH);
			panel_8.add(getPanel_14(), BorderLayout.CENTER);
		}
		return panel_8;
	}
	private JPanel getPanel_13() {
		if (panel_13 == null) {
			panel_13 = new JPanel();
			panel_13.add(getBtnAtras_1());
			panel_13.add(getBtnEmpaquetarProducto());
			panel_13.add(getBtnFactura());
		}
		return panel_13;
	}
	private Panel getPanel_14() {
		if (panel_14 == null) {
			panel_14 = new Panel();
			panel_14.setLayout(new BorderLayout(0, 0));
			panel_14.add(getScrollPane_3(), BorderLayout.CENTER);
		}
		return panel_14;
	}
	private JScrollPane getScrollPane_3() {
		if (scrollPane_3 == null) {
			scrollPane_3 = new JScrollPane();
			scrollPane_3.setBackground(Color.WHITE);
			scrollPane_3.setViewportView(getTablaEmpaquetado());
		}
		return scrollPane_3;
	}
	
	private JButton getBtnFactura() {
		if (btnFactura == null) {
			btnFactura = new JButton("  Generar Factura\r\n  ");
			btnFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					btnValidarPedido.setEnabled(false);
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel3");
					txtResumen.setText(gestor.rellenarInforme(pedidoSeleccionado));
				}
			});
			btnFactura.setOpaque(false);
			btnFactura.setHorizontalAlignment(SwingConstants.RIGHT);
			btnFactura.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnFactura.setEnabled(false);
			btnFactura.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnFactura.setBackground(Color.WHITE);
		}
		return btnFactura;
	}
	private JButton getBtnAnotarIncidencia() {
		if (btnAnotarIncidencia == null) {
			btnAnotarIncidencia = new JButton("Anotar Incidencia");
			btnAnotarIncidencia.setText("  Incidencia  ");
			btnAnotarIncidencia.setOpaque(false);
			btnAnotarIncidencia.setHorizontalAlignment(SwingConstants.RIGHT);
			btnAnotarIncidencia.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnAnotarIncidencia.setEnabled(false);
			btnAnotarIncidencia.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnAnotarIncidencia.setBackground(Color.WHITE);
			btnAnotarIncidencia.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					String incidencia = JOptionPane.showInputDialog("Introduzca Incidencia");
					btnValidarProducto.setEnabled(false);
				}
			});
			btnAnotarIncidencia.setEnabled(false);
		}
		return btnAnotarIncidencia;
	}
	private JButton getBtnAtras() {
		if (btnAtras == null) {
			
			btnAtras = new JButton("Atras");
			btnAtras.setOpaque(false);
			btnAtras.setHorizontalAlignment(SwingConstants.RIGHT);
			btnAtras.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnAtras.setEnabled(true);
			btnAtras.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnAtras.setBackground(Color.WHITE);
			btnAtras.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					reiniciarPn1();
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel0");
				}
			});
		}
		return btnAtras;
	}
	private JButton getBtnEmpaquetar() {
		if (btnEmpaquetar == null) {
			btnEmpaquetar = new JButton("Empaquetar");
			btnEmpaquetar.setOpaque(false);
			btnEmpaquetar.setHorizontalAlignment(SwingConstants.RIGHT);
			btnEmpaquetar.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnEmpaquetar.setEnabled(false);
			btnEmpaquetar.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnEmpaquetar.setBackground(Color.WHITE);
			btnEmpaquetar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					try {
						rellenarTablaEmpaquetado();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
					reiniciarPn1();
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel2");
				}
			});
		}
		return btnEmpaquetar;
	}
	private JButton getBtnEmpaquetarProductosPedido() {
		if (btnEmpaquetarProductosPedido == null) {
			btnEmpaquetarProductosPedido = new JButton("Empaquetar Productos Pedido");
			btnEmpaquetarProductosPedido.setOpaque(false);
			btnEmpaquetarProductosPedido.setHorizontalAlignment(SwingConstants.RIGHT);
			btnEmpaquetarProductosPedido.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnEmpaquetarProductosPedido.setEnabled(false);
			btnEmpaquetarProductosPedido.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnEmpaquetarProductosPedido.setBackground(Color.WHITE);
			btnEmpaquetarProductosPedido.addActionListener(new ActionListener() {
				
				
				public void actionPerformed(ActionEvent e) {
					
					
					reiniciarPn0();
					
					
					try {
						rellenarTablaEmpaquetado();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					
					
					

					if(comprobarTodosEmpaquetados()){
						btnFactura.setEnabled(true);
						btnEmpaquetarProducto.setEnabled(false);
					}
					
					
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel2");
				}
			});
		}
		return btnEmpaquetarProductosPedido;
	}
	private JButton getBtnEmpaquetarProducto() {
		if (btnEmpaquetarProducto == null) {
			btnEmpaquetarProducto = new JButton("Empaquetar Producto");
			btnEmpaquetarProducto.setOpaque(false);
			btnEmpaquetarProducto.setHorizontalAlignment(SwingConstants.RIGHT);
			btnEmpaquetarProducto.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnEmpaquetarProducto.setEnabled(false);
			btnEmpaquetarProducto.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnEmpaquetarProducto.setBackground(Color.WHITE);
			btnEmpaquetarProducto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					try {
						gestor.empaquetarProducto(productoEmpaquetando.getIdProducto(), pedidoSeleccionado.getIdPedido());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					btnEmpaquetarProducto.setEnabled(false);
					reiniciarTablaEmpaquetado();
					
					btnValidarProducto.setEnabled(false);
					if(comprobarTodosEmpaquetados()){
						btnFactura.setEnabled(true);
						btnEmpaquetarProducto.setEnabled(false);
					}
					
				}
			});
		}
		return btnEmpaquetarProducto;
	}
	private JButton getBtnAtras_1() {
		if (btnAtras_1 == null) {
			btnAtras_1 = new JButton(" Atras ");
			btnAtras_1.setOpaque(false);
			btnAtras_1.setHorizontalAlignment(SwingConstants.RIGHT);
			btnAtras_1.setFont(new Font("Book Antiqua", Font.PLAIN, 15));
			btnAtras_1.setEnabled(true);
			btnAtras_1.setBorder(new LineBorder(Color.BLACK, 1, true));
			btnAtras_1.setBackground(Color.WHITE);
			
			
			
			btnAtras_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					
					reiniciarPn2();
					((CardLayout)pnCard.getLayout()).show(pnCard,"panel0");
				}
			});
		}
		return btnAtras_1;
	}
	
	public boolean comprobarTodosValidados(){
		ArrayList<Producto> productos = new ArrayList<Producto>();
		boolean todosValidados=true;
		try {
			productos = gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido());
		} catch (SQLException e) {
			System.out.println("Error de carga de tabla");
			e.printStackTrace();
		}
		
		for(Producto producto : productos){
			try {
				if(!gestor.isProductoValidado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido())){
					todosValidados=false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return todosValidados;
	}
	
	public boolean comprobarTodosEmpaquetados(){
		ArrayList<Producto> productos = new ArrayList<Producto>();
		boolean todosEmpaquetados=true;
		try {
			productos = gestor.rellenarProductosAlmacenero(pedidoSeleccionado.getIdPedido());
		} catch (SQLException e) {
			System.out.println("Error de carga de tabla");
			e.printStackTrace();
		}
		
		for(Producto producto : productos){
			try {
				if(!gestor.isProductoEmpaquetado(producto.getIdProducto(), pedidoSeleccionado.getIdPedido())){
					todosEmpaquetados=false;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return todosEmpaquetados;
	}
}