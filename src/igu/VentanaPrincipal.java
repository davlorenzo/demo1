package igu;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;
import java.awt.CardLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

import javax.swing.border.TitledBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import logic.Cliente;
import logic.Gestor;
import logic.Producto;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ListSelectionModel;
import javax.swing.JPasswordField;

import java.awt.GridLayout;

import javax.swing.JRadioButton;

import org.jvnet.substance.SubstanceLookAndFeel;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class VentanaPrincipal extends JFrame 
{
	private static final long serialVersionUID = 1L;
	
	private JPanel pnPrincipal;
	private JPanel pnInicio;
	private JPanel pnRegistro;
	private JPanel pnCatalogo;
	private JPanel pnCompra;
	private JPanel pnBotones;
	private JButton btVolver;
	private JButton btSiguiente;
	private JPanel pnTablas;
	private JPanel pnInformacion;
	private JPanel pnFiltros;
	private JPanel pnDescripcion;
	private JScrollPane scrollPane;
	private JLabel lbFoto;
	private JTextArea txaDescripcion;
	private JPanel pnCarrito;
	private JLabel lbUsuario;
	private JPanel pnA�adir;
	private JPanel pnBotonesA�adir;
	private JPanel pnPrecio;
	private JLabel lbPrecio;
	private JTextField txPrecio;
	private JLabel lbUnidades;
	private JPanel pnBotones2;
	private JButton btA�adir;
	private JPanel pnDetalles;
	private JButton btCatalogo;
	private JSpinner spinner;
	
	private Gestor gestor;
	
	private String[] columnas = {"Nombre", "Tipo", "Precio"};
	private String[] columnasCarrito = {"Nombre", "Cantidad", "Precio"};
	
	private JTable tbProductos;
	private JTable tbCarrito;
	private ModeloNoEditable modeloTabla;
	private ModeloNoEditable modeloTablaCarrito = new ModeloNoEditable(columnasCarrito, 0);
	int filaSeleccionada = 0;
	
	private JPanel pnProcesado;
	private JPanel pnBoton;
	private JButton btFinalizar;
	private JPanel panel;
	private JButton btVolver2;
	private JButton btEliminar;
	private JButton btVaciarCarrito;
	private JScrollPane scrollPane_1;
	private JLabel lbUser;
	private JTextField txUser;
	private JLabel lbPass;
	private JPasswordField psPass;
	private JButton btIniciarSesion;
	private JButton btRegistro;
	private JLabel lbUsuarioRegistrado;
	
	private Cliente clienteActual;
	private JPanel panel_1;
	private JLabel lbConfirmacionPedido;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_5;
	private JPanel panel_6;
	private JPanel panel_7;
	private JPanel panel_8;
	private JPanel panel_9;
	private JPanel panel_10;
	private JPanel panel_11;
	private JPanel panel_12;
	private JLabel lbNombre;
	private JTextField txNombre;
	private JLabel lbDNI;
	private JTextField txDNI;
	private JLabel lbTelefono;
	private JTextField txTelefono;
	private JLabel lbDireccion;
	private JTextField txDireccion;
	private JLabel lbTarjeta;
	private JTextField txTarjeta;
	private JLabel lbCuenta;
	private JTextField txCuenta;
	private JLabel lbAgradecimiento;
	private JLabel lbVariable;
	private JLabel lbEuros;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
					
					//SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.BusinessSkin");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public VentanaPrincipal() throws SQLException
	{
		gestor = new Gestor();

		setTitle("Tienda de Electr\u00F3nica");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1185, 621);
		pnPrincipal = new JPanel();
		pnPrincipal.setBackground(Color.WHITE);
		pnPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnPrincipal);
		pnPrincipal.setLayout(new CardLayout(0, 0));
		pnPrincipal.add(getPnInicio(), "inicio");
		pnPrincipal.add(getPnCompra(), "tabla");
		pnPrincipal.add(getPnProcesado(), "procesado");
		
		setLocationRelativeTo(null);
		
		mostrarTabla();
	}
	
	public JTextField devolverTxPrecio()
	{
		return txPrecio;
	}
	
	public JPanel devolverPanel()
	{
		return pnPrincipal;
	}
	
	private void mostrarTabla()
	{
		modeloTabla = new ModeloNoEditable(columnas, 0);
		
		for(Producto p: gestor.getProductosTotales())
		{
			String[] fila = new String[3];
			
			fila[0] = p.getNombre();
			fila[1] = p.getTipo();
			fila[2] = String.valueOf(p.getPrecio());
			
			modeloTabla.addRow(fila);
		}
		
		tbProductos.getTableHeader().setReorderingAllowed(false);
		tbProductos.setModel(modeloTabla);
	}
	
	private void rellenarTablaCarrito() {
		Object[] celda = new Object[3];
		int filaSelec = tbProductos.getSelectedRow();
		boolean existe = false;
		if (filaSelec >= 0) {
			String nombreProducto = (String) modeloTabla.getValueAt(filaSelec, 0);
			double precio = Double.valueOf((String) modeloTabla.getValueAt(filaSelec, 2));
			if (modeloTablaCarrito.getRowCount() > 0) {
				for (int i=0; i<modeloTablaCarrito.getRowCount(); i++) {
					if (nombreProducto.equals(modeloTablaCarrito.getValueAt(i, 0))) {
						int value = (int) modeloTablaCarrito.getValueAt(i, 1);
						int cantidad = (int)spinner.getValue() + value;
						modeloTablaCarrito.setValueAt(cantidad, i, 1);
						modeloTablaCarrito.setValueAt(cantidad * precio, i, 2);
						existe = true;
					}
				}
				if (!existe) {
					celda[0] = nombreProducto;
					celda[1] = (int)spinner.getValue();
					celda[2] = precio * (int)spinner.getValue();
					modeloTablaCarrito.addRow(celda);
				}
			}
			else {
				celda[0] = nombreProducto;
				celda[1] = (int)spinner.getValue();
				celda[2] = Double.valueOf((String) modeloTabla.getValueAt(filaSelec, 2)) * (int)spinner.getValue();
				modeloTablaCarrito.addRow(celda);
			}
		}
		tbCarrito.getTableHeader().setReorderingAllowed(false);
		tbCarrito.setModel(modeloTablaCarrito);
	}
	
	private JPanel getPnInicio() {
		if (pnInicio == null) {
			pnInicio = new JPanel();
			pnInicio.setBackground(Color.WHITE);
			pnInicio.setLayout(new BorderLayout(0, 0));
			pnInicio.add(getPnRegistro(), BorderLayout.NORTH);
			pnInicio.add(getPnCatalogo(), BorderLayout.CENTER);
		}
		return pnInicio;
	}
	private JPanel getPnRegistro() {
		if (pnRegistro == null) {
			pnRegistro = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnRegistro.getLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			pnRegistro.setBackground(Color.WHITE);
			pnRegistro.add(getLbUser());
			pnRegistro.add(getTxUser());
			pnRegistro.add(getLbPass());
			pnRegistro.add(getPsPass());
			pnRegistro.add(getBtIniciarSesion());
			pnRegistro.add(getBtRegistro());
		}
		return pnRegistro;
	}
	private JPanel getPnCatalogo() {
		if (pnCatalogo == null) {
			pnCatalogo = new JPanel();
			pnCatalogo.setBackground(Color.WHITE);
			pnCatalogo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 165));
			pnCatalogo.add(getBtCatalogo());
		}
		return pnCatalogo;
	}
	private JPanel getPnCompra() {
		if (pnCompra == null) {
			pnCompra = new JPanel();
			pnCompra.setBackground(Color.WHITE);
			pnCompra.setLayout(new BorderLayout(0, 0));
			pnCompra.add(getPnBotones(), BorderLayout.SOUTH);
			pnCompra.add(getPnTablas(), BorderLayout.CENTER);
			pnCompra.add(getPnInformacion(), BorderLayout.EAST);
		}
		return pnCompra;
	}
	private JPanel getPnBotones() {
		if (pnBotones == null) {
			pnBotones = new JPanel();
			pnBotones.setBackground(Color.WHITE);
			pnBotones.setLayout(new BorderLayout(0, 0));
			pnBotones.add(getBtVolver(), BorderLayout.WEST);
			pnBotones.add(getBtSiguiente(), BorderLayout.EAST);
		}
		return pnBotones;
	}
	private JButton getBtVolver() {
		if (btVolver == null) {
			btVolver = new JButton("Volver");
			btVolver.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "inicio");
					
					txPrecio.setText("");
					
					txaDescripcion.setText("");
					lbFoto.setIcon(null);
					
					btA�adir.setEnabled(false);
					spinner.setValue(1);
					
					gestor.vaciarProductosCarrito();
					gestor.precioCarrito();
					
					txUser.setText("");
					psPass.setText("");
					
					pnCarrito.remove(2);
					
					for(int i = 0; i < modeloTablaCarrito.getRowCount(); i ++)
					{
						modeloTablaCarrito.removeRow(i);
					}
				}
			});
			btVolver.setMnemonic('V');
			btVolver.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return btVolver;
	}
	private JButton getBtSiguiente() {
		if (btSiguiente == null) {
			btSiguiente = new JButton("Siguiente");
			btSiguiente.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					if(!gestor.getProductosCarrito().isEmpty())
					{
						((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "procesado");
						
						if(clienteActual != null)
						{
							txNombre.setText(clienteActual.getNombre());
							txDNI.setText(clienteActual.getDni());
							txTelefono.setText(clienteActual.getTelefono());
							txDireccion.setText(clienteActual.getDireccion());
							txTarjeta.setText(clienteActual.getNTarjeta());
							txCuenta.setText(clienteActual.getNCuenta());
						}
						else
						{
							lbVariable.setText("Por favor, introduzca sus datos para la realizaci�n de la compra");
						}
					}
					else
					{
						mostrarError();
					}
				}
			});
			btSiguiente.setEnabled(false);
			btSiguiente.setMnemonic('S');
		}
		return btSiguiente;
	}
	
	private void mostrarError()
	{
		JOptionPane.showMessageDialog(this, "Debe a�adir alg�n producto al carrito para poder continuar", "Error de compra", 2);
	}
	
	private JPanel getPnTablas() {
		if (pnTablas == null) {
			pnTablas = new JPanel();
			pnTablas.setBackground(Color.WHITE);
			pnTablas.setLayout(new BorderLayout(0, 0));
			pnTablas.add(getPnFiltros(), BorderLayout.NORTH);
			pnTablas.add(getPnDescripcion(), BorderLayout.SOUTH);
			pnTablas.add(getScrollPane(), BorderLayout.CENTER);
		}
		return pnTablas;
	}
	private JPanel getPnInformacion() {
		if (pnInformacion == null) {
			pnInformacion = new JPanel();
			pnInformacion.setBorder(new TitledBorder(null, "Su pedido", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnInformacion.setBackground(Color.WHITE);
			pnInformacion.setLayout(new BorderLayout(0, 0));
			pnInformacion.add(getPnCarrito(), BorderLayout.NORTH);
			pnInformacion.add(getPnA�adir(), BorderLayout.SOUTH);
			pnInformacion.add(getPnDetalles(), BorderLayout.CENTER);
		}
		return pnInformacion;
	}
	private JPanel getPnFiltros() {
		if (pnFiltros == null) {
			pnFiltros = new JPanel();
			pnFiltros.setBorder(new TitledBorder(null, "Filtros", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnFiltros.setBackground(Color.WHITE);
		}
		return pnFiltros;
	}
	private JPanel getPnDescripcion() {
		if (pnDescripcion == null) {
			pnDescripcion = new JPanel();
			pnDescripcion.setBorder(new TitledBorder(null, "Descripcion del producto", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnDescripcion.setBackground(Color.WHITE);
			pnDescripcion.setLayout(new BorderLayout(0, 0));
			pnDescripcion.add(getLbFoto(), BorderLayout.EAST);
			pnDescripcion.add(getTxaDescripcion(), BorderLayout.CENTER);
		}
		return pnDescripcion;
	}
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTbProductos());
		}
		return scrollPane;
	}
	private JTable getTbProductos() {
		if (tbProductos == null) {
			tbProductos = new JTable();
			tbProductos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tbProductos.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0)
				{
					spinner.setValue(1);
					
					filaSeleccionada = tbProductos.getSelectedRow();
					
					gestor.seleccionarProducto(filaSeleccionada);

					txaDescripcion.setText(gestor.getProductoSeleccionado().getDescripcion() + "\n" + "\n" + "Nombre del producto: " + gestor.getProductoSeleccionado().getNombre() +
							"\n" + "Tipo de producto: " + gestor.getProductoSeleccionado().getTipo() + "\n" + "Precio de este producto: " + gestor.getProductoSeleccionado().getPrecio());
					
					String imagen = gestor.getProductoSeleccionado().getImagen();
					
					lbFoto.setIcon(new ImageIcon(VentanaPrincipal.class.getResource(imagen)));
					
					btA�adir.setEnabled(true);
					
					if (gestor.getProductosCarrito().isEmpty())
					{
						btSiguiente.setEnabled(false);
					}
					else
					{
						btSiguiente.setEnabled(true);
					}
				}
			});
		}
		return tbProductos;
	}
	
	private JButton getBtA�adir() {
		if (btA�adir == null) {
			btA�adir = new JButton("A\u00F1adir");
			btA�adir.setEnabled(false);
			btA�adir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0)
				{
					int fila = tbProductos.getSelectedRow();
					gestor.seleccionarProducto(fila);
					
					gestor.a�adirProductoCarrito(gestor.getProductoSeleccionado(), (int) spinner.getValue());
					rellenarTablaCarrito();
					
					txPrecio.setText(String.valueOf(gestor.precioCarrito()));
					
					btSiguiente.setEnabled(true);
				}
			});
			btA�adir.setToolTipText("A\u00F1ade las unidades seleccionadas en el spinner al carrito");
			btA�adir.setMnemonic('A');
		}
		return btA�adir;
	}
	private JLabel getLbFoto() {
		if (lbFoto == null) {
			lbFoto = new JLabel("");
		}
		return lbFoto;
	}
	private JTextArea getTxaDescripcion() {
		if (txaDescripcion == null) {
			txaDescripcion = new JTextArea();
			txaDescripcion.setEditable(false);
			txaDescripcion.setBackground(Color.WHITE);
			txaDescripcion.setLineWrap(true);
			txaDescripcion.setWrapStyleWord(true);
		}
		return txaDescripcion;
	}
	private JPanel getPnCarrito() {
		if (pnCarrito == null) {
			pnCarrito = new JPanel();
			pnCarrito.setBackground(Color.WHITE);
			pnCarrito.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 5));
			pnCarrito.add(getLbUsuario());
			pnCarrito.add(getLbUsuarioRegistrado());
		}
		return pnCarrito;
	}
	private JLabel getLbUsuario() {
		if (lbUsuario == null) {
			lbUsuario = new JLabel("Usuario:");
			lbUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbUsuario;
	}
	private JPanel getPnA�adir() {
		if (pnA�adir == null) {
			pnA�adir = new JPanel();
			pnA�adir.setBackground(Color.WHITE);
			pnA�adir.setLayout(new BorderLayout(0, 0));
			pnA�adir.add(getPnBotonesA�adir(), BorderLayout.NORTH);
			pnA�adir.add(getPnPrecio(), BorderLayout.SOUTH);
		}
		return pnA�adir;
	}
	private JPanel getPnBotonesA�adir() {
		if (pnBotonesA�adir == null) {
			pnBotonesA�adir = new JPanel();
			pnBotonesA�adir.setBackground(Color.WHITE);
			pnBotonesA�adir.setLayout(new BorderLayout(0, 0));
			pnBotonesA�adir.add(getLbUnidades(), BorderLayout.NORTH);
			pnBotonesA�adir.add(getPnBotones2(), BorderLayout.SOUTH);
		}
		return pnBotonesA�adir;
	}
	private JPanel getPnPrecio() {
		if (pnPrecio == null) {
			pnPrecio = new JPanel();
			pnPrecio.setBackground(Color.WHITE);
			pnPrecio.add(getLbPrecio());
			pnPrecio.add(getTxPrecio());
			pnPrecio.add(getLbEuros());
		}
		return pnPrecio;
	}
	private JLabel getLbPrecio() {
		if (lbPrecio == null) {
			lbPrecio = new JLabel("Precio total del pedido");
		}
		return lbPrecio;
	}
	private JTextField getTxPrecio() {
		if (txPrecio == null) {
			txPrecio = new JTextField();
			txPrecio.setEditable(false);
			txPrecio.setColumns(10);
		}
		return txPrecio;
	}
	private JLabel getLbUnidades() {
		if (lbUnidades == null) {
			lbUnidades = new JLabel("Unidades");
			lbUnidades.setHorizontalAlignment(SwingConstants.CENTER);
			lbUnidades.setDisplayedMnemonic('U');
		}
		return lbUnidades;
	}
	private JPanel getPnBotones2() {
		if (pnBotones2 == null) {
			pnBotones2 = new JPanel();
			pnBotones2.setBackground(Color.WHITE);
			pnBotones2.add(getSpinner());
			pnBotones2.add(getBtA�adir());
			pnBotones2.add(getBtEliminar());
			pnBotones2.add(getBtVaciarCarrito());
		}
		return pnBotones2;
	}

	private JPanel getPnDetalles() {
		if (pnDetalles == null) {
			pnDetalles = new JPanel();
			pnDetalles.setBackground(Color.WHITE);
			pnDetalles.setLayout(new BorderLayout(0, 0));
			pnDetalles.add(getScrollPane_1(), BorderLayout.CENTER);
		}
		return pnDetalles;
	}
	private JButton getBtCatalogo() {
		if (btCatalogo == null) {
			btCatalogo = new JButton("Catalogo");
			btCatalogo.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "tabla");
					crearBotonesPanel(pnCarrito);
					if(clienteActual != null)
					{
						lbUsuarioRegistrado.setText(clienteActual.getUsuario());
					}
					else
					{
						lbUsuarioRegistrado.setText("");
					}
				}
			});
			btCatalogo.setFont(new Font("Tahoma", Font.BOLD, 18));
			btCatalogo.setMnemonic('C');
		}
		return btCatalogo;
	}
	private JSpinner getSpinner() {
		if (spinner == null) {
			spinner = new JSpinner();
			spinner.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		}
		return spinner;
	}
	
	public Gestor devolverGestor()
	{
		return gestor;
	}
	
	private JPanel getPnProcesado() {
		if (pnProcesado == null) {
			pnProcesado = new JPanel();
			pnProcesado.setBackground(Color.WHITE);
			pnProcesado.setLayout(new BorderLayout(0, 0));
			pnProcesado.add(getPnBoton(), BorderLayout.SOUTH);
			pnProcesado.add(getPanel(), BorderLayout.CENTER);
		}
		return pnProcesado;
	}
	private JPanel getPnBoton() {
		if (pnBoton == null) {
			pnBoton = new JPanel();
			pnBoton.setBackground(Color.WHITE);
			pnBoton.setLayout(new BorderLayout(0, 0));
			pnBoton.add(getBtFinalizar(), BorderLayout.EAST);
			pnBoton.add(getBtVolver2(), BorderLayout.WEST);
		}
		return pnBoton;
	}
	private JButton getBtFinalizar() {
		if (btFinalizar == null) {
			btFinalizar = new JButton("Finalizar");
			btFinalizar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					try 
					{
						if(comprobarCamposVacios())
						{
							gestor.registrarCliente(null, null, txNombre.getText(), txDNI.getText(), txDireccion.getText(), txTelefono.getText(), txCuenta.getText(), txTarjeta.getText(), false);
							registrarPedido();
							for (Producto p: gestor.getProductosCarrito()) 
							{
								registrarCompra(p.getIdProducto());
							}
							
							clienteActual = null;
							for(int i = 0; i < modeloTablaCarrito.getRowCount(); i ++)
							{
								modeloTablaCarrito.removeRow(i);
							}
							
							txUser.setText("");
							psPass.setText("");
							
							reiniciar();
						}
						else
						{
							mostrarAvisoCamposVacios();
						}
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
					}
				}
			});
			btFinalizar.setMnemonic('F');
		}
		return btFinalizar;
	}
	
	private boolean comprobarCamposVacios()
	{
		if(txNombre.getText().equals("") || txNombre.getText().equals(" ") || txDNI.getText().equals("") || txDNI.getText().equals(" ") || txDireccion.getText().equals("") || txDireccion.getText().equals(" ")
				|| txTelefono.getText().equals("") || txTelefono.getText().equals(" "))
		{
			return false;
		}
		else if((txCuenta.getText().equals("") || txCuenta.getText().equals(" ")) && (txTarjeta.getText().equals("") || txTarjeta.getText().equals(" ")))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private void mostrarAvisoCamposVacios()
	{
		JOptionPane.showMessageDialog(this, "Por favor, rellene todos los campos para continuar", "Error en el registro", 2);
	}
	
	private void registrarPedido() throws SQLException {
		Connection con = Gestor.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO PEDIDO VALUES(?, ?, ?, FALSE, NULL)");
		
		int numFilas = contarFilas() + 1;
		ps.setInt(1, numFilas);
		ps.setDouble(2, Double.valueOf(txPrecio.getText()));
		Date fechaActual = new Date();
		DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
        String fecha = formatoFecha.format(fechaActual);
        ps.setString(3, fecha);
		
		int n = ps.executeUpdate();
	}
	
	private int contarFilas() throws SQLException {
		Connection con = Gestor.getConnection();
		Statement ps = con.createStatement();
		ResultSet rs = ps.executeQuery("SELECT COUNT(*) AS NUM FROM PEDIDO");
		
		int num = 0;
		while (rs.next()) {
			num = rs.getInt("num");
		}
		return num;
	}
	
	private void registrarCompra(int id) throws SQLException {
		Connection con = Gestor.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO COMPRA VALUES(NULL, ?, ?, false, false)");
		
		ps.setInt(1, id);
		int numFilas = contarFilas();
		ps.setInt(2, numFilas);
		
		int n = ps.executeUpdate();	
	}
	
	private void reiniciar()
	{
		gestor.vaciarProductosCarrito();
		
		((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "inicio");
		
		txPrecio.setText("");
		
		txaDescripcion.setText("");
		lbFoto.setIcon(null);
		
		btA�adir.setEnabled(false);
		spinner.setValue(1);
		
		txNombre.setText("");
		txDNI.setText("");
		txTelefono.setText("");
		txDireccion.setText("");
		txTarjeta.setText("");
		txCuenta.setText("");
		
		txUser.setText("");
		psPass.setText("");
	
		pnCarrito.remove(2);
		
		clienteActual = null;
	}
	
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBackground(Color.WHITE);
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanel_1(), BorderLayout.NORTH);
			panel.add(getPanel_2(), BorderLayout.CENTER);
		}
		return panel;
	}
	private JButton getBtVolver2() {
		if (btVolver2 == null) {
			btVolver2 = new JButton("Volver");
			btVolver2.setMnemonic('V');
			btVolver2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "tabla");
					
					txNombre.setText("");
					txDNI.setText("");
					txTelefono.setText("");
					txDireccion.setText("");
					txTarjeta.setText("");
					txCuenta.setText("");
				}
			});
		}
		return btVolver2;
	}
	private JButton getBtEliminar() {
		if (btEliminar == null) {
			btEliminar = new JButton("Eliminar");
			btEliminar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					int filaSeleccionada = tbCarrito.getSelectedRow();
					int cantidad = (int) modeloTablaCarrito.getValueAt(filaSeleccionada, 1);
					
					gestor.seleccionarProductoCarrito(filaSeleccionada);;
					
					if (cantidad == (int)spinner.getValue())
					{
						modeloTablaCarrito.removeRow(filaSeleccionada);
						double precio = 0;
						
						for(int i = 0; i < (int)spinner.getValue(); i ++)
						{
							gestor.eliminarProductoCarrito(gestor.getProductoSeleccionadoCarrito());
						}
						
						for(int i = 0; i < modeloTablaCarrito.getRowCount(); i ++)
						{
							precio = precio + (double) modeloTablaCarrito.getValueAt(i, 2);
						}
						
						txPrecio.setText(String.valueOf(precio));
					}
					else if (cantidad > (int)spinner.getValue()) 
					{
						double precioCarrito = 0;
						
						double precio = (double) modeloTablaCarrito.getValueAt(filaSeleccionada, 2) / cantidad;
						precio = gestor.redondearDecimales(precio, 2);
						modeloTablaCarrito.setValueAt(cantidad - (int)spinner.getValue(), filaSeleccionada, 1);
						modeloTablaCarrito.setValueAt(gestor.redondearDecimales((int) modeloTablaCarrito.getValueAt(filaSeleccionada, 1) * precio, 2), filaSeleccionada, 2);
						
						for(int i = 0; i < (int)spinner.getValue(); i ++)
						{
							gestor.eliminarProductoCarrito(gestor.getProductoSeleccionadoCarrito());
						}
						
						for(int i = 0; i < modeloTablaCarrito.getRowCount(); i ++)
						{
							precioCarrito = precioCarrito + (double) modeloTablaCarrito.getValueAt(i, 2);
						}
						
						txPrecio.setText(String.valueOf(precioCarrito));
					}
					else 
					{
						JOptionPane.showMessageDialog(tbCarrito, "No se puede borrar una cantidad mayor a la existente");
					}
				}
			});
			btEliminar.setEnabled(false);
			btEliminar.setToolTipText("Elimina las unidades seleccionadas en el spinner del carrito");
			btEliminar.setMnemonic('E');
		}
		return btEliminar;
	}
	private JButton getBtVaciarCarrito() {
		if (btVaciarCarrito == null) {
			btVaciarCarrito = new JButton("Vaciar Carrito");
			btVaciarCarrito.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					txPrecio.setText("0.0");
					gestor.vaciarProductosCarrito();
					reiniciarCarrito();
				}
			});
			btVaciarCarrito.setMnemonic('V');
			btVaciarCarrito.setToolTipText("Elimina todo el contenido del carrito");
		}
		return btVaciarCarrito;
	}
	
	private void reiniciarCarrito() {
		int numFilas = modeloTablaCarrito.getRowCount()-1;
		for (int i=numFilas; i>=0; i--) {
			modeloTablaCarrito.removeRow(i);
		}
	}
	
	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setViewportView(getTbCarrito());
		}
		return scrollPane_1;
	}
	private JTable getTbCarrito() {
		if (tbCarrito == null) {
			tbCarrito = new JTable();
			tbCarrito.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					btEliminar.setEnabled(true);
				}
			});
		}
		return tbCarrito;
	}
	private JLabel getLbUser() {
		if (lbUser == null) {
			lbUser = new JLabel("Usuario:");
			lbUser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbUser;
	}
	private JTextField getTxUser() {
		if (txUser == null) {
			txUser = new JTextField();
			txUser.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txUser.setColumns(10);
		}
		return txUser;
	}
	private JLabel getLbPass() {
		if (lbPass == null) {
			lbPass = new JLabel("Contrase\u00F1a:");
			lbPass.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbPass;
	}
	private JPasswordField getPsPass() {
		if (psPass == null) {
			psPass = new JPasswordField();
			psPass.setColumns(10);
			psPass.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return psPass;
	}
	private JButton getBtIniciarSesion() {
		if (btIniciarSesion == null) {
			btIniciarSesion = new JButton("Iniciar Sesion");
			btIniciarSesion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) 
				{
					String usuario = txUser.getText();
					char[] contrase�aCodex = psPass.getPassword();
					
					String contrase�a = "";
					
					for(int i = 0; i < contrase�aCodex.length; i ++)
					{
						contrase�a = contrase�a + contrase�aCodex[i];
					}
					
					if(gestor.comprobarContrase�as(usuario, contrase�a))
					{
						((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "tabla");
						lbUsuarioRegistrado.setText(usuario);
						
						try 
						{
							clienteActual = gestor.getClienteActual(usuario);
							crearBotonesPanel(pnCarrito);
						} catch (SQLException e) 
						{
							e.printStackTrace();
						}
						
						System.out.println(clienteActual.getNombre());
					}
					else
					{
						txUser.setText("");
						psPass.setText("");
						mostrarAvisoError();
					}
				}
			});
			btIniciarSesion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			btIniciarSesion.setMnemonic('I');
		}
		return btIniciarSesion;
	}
	
	private void mostrarAvisoError()
	{
		JOptionPane.showMessageDialog(this, "El usuario y/o la contrase�a no son correctos", "Datos incorrectos", 2);
	}
	
	private JButton getBtRegistro() {
		if (btRegistro == null) {
			btRegistro = new JButton("Registrarse");
			btRegistro.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					mostrarVentanaRegistro();
				}
			});
			btRegistro.setMnemonic('R');
			btRegistro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return btRegistro;
	}
	
	private void mostrarVentanaRegistro()
	{
		VentanaRegistro vr = new VentanaRegistro(this);
		
		vr.setVisible(true);
		vr.setLocationRelativeTo(this);
	}
	
	private JLabel getLbUsuarioRegistrado() {
		if (lbUsuarioRegistrado == null) {
			lbUsuarioRegistrado = new JLabel("");
			lbUsuarioRegistrado.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lbUsuarioRegistrado;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBackground(Color.WHITE);
			panel_1.add(getLbConfirmacionPedido());
		}
		return panel_1;
	}
	private JLabel getLbConfirmacionPedido() {
		if (lbConfirmacionPedido == null) {
			lbConfirmacionPedido = new JLabel("Gracias por su compra");
			lbConfirmacionPedido.setFont(lbConfirmacionPedido.getFont().deriveFont(lbConfirmacionPedido.getFont().getSize() + 20f));
		}
		return lbConfirmacionPedido;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.setBackground(Color.WHITE);
			panel_2.setLayout(new BorderLayout(0, 0));
			panel_2.add(getPanel_3(), BorderLayout.NORTH);
			panel_2.add(getPanel_4(), BorderLayout.CENTER);
		}
		return panel_2;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setBackground(Color.WHITE);
			panel_3.setLayout(new BorderLayout(0, 0));
		}
		return panel_3;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.setBackground(Color.WHITE);
			panel_4.setLayout(new GridLayout(8, 0, 0, 0));
			panel_4.add(getPanel_6());
			panel_4.add(getPanel_7());
			panel_4.add(getPanel_8());
			panel_4.add(getPanel_9());
			panel_4.add(getPanel_10());
			panel_4.add(getPanel_11());
			panel_4.add(getPanel_12());
			panel_4.add(getPanel_5());
		}
		return panel_4;
	}
	private JPanel getPanel_5() {
		if (panel_5 == null) {
			panel_5 = new JPanel();
			panel_5.setBackground(Color.WHITE);
			panel_5.add(getLbAgradecimiento());
		}
		return panel_5;
	}
	private JPanel getPanel_6() {
		if (panel_6 == null) {
			panel_6 = new JPanel();
			panel_6.setBackground(Color.WHITE);
			panel_6.add(getLbVariable());
		}
		return panel_6;
	}
	private JPanel getPanel_7() {
		if (panel_7 == null) {
			panel_7 = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel_7.getLayout();
			panel_7.setBackground(Color.WHITE);
			panel_7.add(getLbNombre());
			panel_7.add(getTxNombre());
		}
		return panel_7;
	}
	private JPanel getPanel_8() {
		if (panel_8 == null) {
			panel_8 = new JPanel();
			panel_8.setBackground(Color.WHITE);
			panel_8.add(getLbDNI());
			panel_8.add(getTxDNI());
		}
		return panel_8;
	}
	private JPanel getPanel_9() {
		if (panel_9 == null) {
			panel_9 = new JPanel();
			panel_9.setBackground(Color.WHITE);
			panel_9.add(getLbTelefono());
			panel_9.add(getTxTelefono());
		}
		return panel_9;
	}
	private JPanel getPanel_10() {
		if (panel_10 == null) {
			panel_10 = new JPanel();
			panel_10.setBackground(Color.WHITE);
			panel_10.add(getLbDireccion());
			panel_10.add(getTxDireccion());
		}
		return panel_10;
	}
	private JPanel getPanel_11() {
		if (panel_11 == null) {
			panel_11 = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel_11.getLayout();
			panel_11.setBackground(Color.WHITE);
			panel_11.add(getLbTarjeta());
			panel_11.add(getTxTarjeta());
		}
		return panel_11;
	}
	private JPanel getPanel_12() {
		if (panel_12 == null) {
			panel_12 = new JPanel();
			panel_12.setBackground(Color.WHITE);
			panel_12.add(getLbCuenta());
			panel_12.add(getTxCuenta());
		}
		return panel_12;
	}
	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre:");
			lbNombre.setDisplayedMnemonic('N');
			lbNombre.setLabelFor(getTxNombre());
			lbNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbNombre;
	}
	private JTextField getTxNombre() {
		if (txNombre == null) {
			txNombre = new JTextField();
			txNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txNombre.setColumns(25);
		}
		return txNombre;
	}
	private JLabel getLbDNI() {
		if (lbDNI == null) {
			lbDNI = new JLabel("DNI o NIF:");
			lbDNI.setDisplayedMnemonic('D');
			lbDNI.setLabelFor(getTxDNI());
			lbDNI.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbDNI;
	}
	private JTextField getTxDNI() {
		if (txDNI == null) {
			txDNI = new JTextField();
			txDNI.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) 
				{
					String dni = txDNI.getText();
					
					if(dni.length() != 9)
					{
						txDNI.setText("");
					}
					if(comprobarDNI() == false)
					{
						txDNI.setText("");
					}
				}
			});
			txDNI.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDNI.setColumns(24);
		}
		return txDNI;
	}
	
	private boolean comprobarDNI()
	{
        char[] letraDni = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D',  'X',  'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};  
  
        String num = "";
        int ind = 0;  
 
        String dni = txDNI.getText();
  
        if(dni.length() == 8) 
        {
             dni = "0" + dni;
        }
        if (!Character.isLetter(dni.charAt(8)))
        {
             return false;  
        }
        if (dni.length() != 9)
        {   
             return false;
        }  
        for (int i = 0; i < 8; i ++) 
        {
             if(!Character.isDigit(dni.charAt(i)))
             {
                   return false;    
             }

             num += dni.charAt(i);     
        }
  
        ind = Integer.parseInt(num);
  
        ind %= 23;
  
        if ((Character.toUpperCase(dni.charAt(8))) != letraDni[ind])
        {
             return false;
        }  
        
        return true; 
	}
	
	private JLabel getLbTelefono() {
		if (lbTelefono == null) {
			lbTelefono = new JLabel("Tel\u00E9fono:");
			lbTelefono.setDisplayedMnemonic('T');
			lbTelefono.setLabelFor(getTxTelefono());
			lbTelefono.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbTelefono;
	}
	private JTextField getTxTelefono() {
		if (txTelefono == null) {
			txTelefono = new JTextField();
			txTelefono.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) 
				{
					boolean error = false;
					try
					{
						Integer.parseInt(txTelefono.getText());
						
					}catch(NumberFormatException e)
					{
						error = true;
					}
					
					if(error || txTelefono.getText().length() != 9)
					{
						txTelefono.setText("");
					}
				}
			});
			txTelefono.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txTelefono.setColumns(25);
		}
		return txTelefono;
	}
	private JLabel getLbDireccion() {
		if (lbDireccion == null) {
			lbDireccion = new JLabel("Direcci\u00F3n:");
			lbDireccion.setDisplayedMnemonic('r');
			lbDireccion.setLabelFor(getTxDireccion());
			lbDireccion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbDireccion;
	}
	private JTextField getTxDireccion() {
		if (txDireccion == null) {
			txDireccion = new JTextField();
			txDireccion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDireccion.setColumns(25);
		}
		return txDireccion;
	}
	private JLabel getLbTarjeta() {
		if (lbTarjeta == null) {
			lbTarjeta = new JLabel("N\u00FAmero de la Tarjeta:");
			lbTarjeta.setDisplayedMnemonic('j');
			lbTarjeta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbTarjeta.setLabelFor(getTxTarjeta());
		}
		return lbTarjeta;
	}
	private JTextField getTxTarjeta() {
		if (txTarjeta == null) {
			txTarjeta = new JTextField();
			txTarjeta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) 
				{
					boolean error = false;
					
					try
					{
						Float.parseFloat(txTarjeta.getText());
						
					}catch(NumberFormatException e1)
					{
						error = true;
					}
					
					if(error || txTarjeta.getText().length() != 16)
					{
						txTarjeta.setText("");
					}
				}
			});
			txTarjeta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txTarjeta.setColumns(20);
		}
		return txTarjeta;
	}
	private JLabel getLbCuenta() {
		if (lbCuenta == null) {
			lbCuenta = new JLabel("N\u00FAmero de Cuenta:");
			lbCuenta.setDisplayedMnemonic('C');
			lbCuenta.setLabelFor(getTxCuenta());
			lbCuenta.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbCuenta;
	}
	private JTextField getTxCuenta() {
		if (txCuenta == null) {
			txCuenta = new JTextField();
			txCuenta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) 
				{
					boolean error = false;
					
					try
					{
						Float.parseFloat(txCuenta.getText());
						
					}catch(NumberFormatException e1)
					{
						error = true;
					}
					
					if(error || txCuenta.getText().length() != 24)
					{
						txCuenta.setText("");
					}
				}
			});
			txCuenta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txCuenta.setColumns(22);
		}
		return txCuenta;
	}
	private JLabel getLbAgradecimiento() {
		if (lbAgradecimiento == null) {
			lbAgradecimiento = new JLabel("Pulse finalizar para confirmar su compra una vez todos los campos hayan sido rellenados");
			lbAgradecimiento.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lbAgradecimiento;
	}
	private JLabel getLbVariable() {
		if (lbVariable == null) {
			lbVariable = new JLabel("Estos son sus datos de registro:");
			lbVariable.setHorizontalAlignment(SwingConstants.CENTER);
			lbVariable.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lbVariable;
	}
	private JLabel getLbEuros() {
		if (lbEuros == null) {
			lbEuros = new JLabel("\u20AC");
			lbEuros.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lbEuros;
	}
	
	private void crearBotonesPanel(JPanel panel)
	{
		panel.add(nuevoBoton());
	}
	
	private JButton nuevoBoton() 
	{
		JButton boton;
		
		if(clienteActual != null)
		{
			boton = new JButton("Cerrar Sesion");
			boton.setEnabled(true);
			boton.setMnemonic('C');
			
			boton.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent e) 
				{
					clienteActual = null;
					pnCarrito.remove(2);
					crearBotonesPanel(pnCarrito);
					lbUsuarioRegistrado.setText("");
				}
			});
			
			return boton;
		}
		else
		{
			boton = new JButton("Iniciar Sesion");
			boton.setEnabled(true);
			boton.setMnemonic('I');
			
			boton.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent e) 
				{
					((CardLayout)pnPrincipal.getLayout()).show(pnPrincipal, "inicio");
					pnCarrito.remove(2);
				}
			});
			
			return boton;
		}
	}
}