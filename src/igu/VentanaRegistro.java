package igu;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logic.Cliente;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.sql.SQLException;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class VentanaRegistro extends JDialog 
{
	private static final long serialVersionUID = 1L;
	
	private JPanel pnContenido;
	private JPanel panel;
	private JPanel pnCarrito;
	private JButton btCerrar;
	private JButton btRegistrarse;
	
	private VentanaPrincipal vp;

	private JLabel lbTitulo;
	private JLabel lbNombre;
	private JTextField txNombre;
	private JLabel lbDNI;
	private JTextField txDNI;
	private JLabel lbDireccion;
	private JTextField txDireccion;
	private JTextField txTelefono;
	private JTextField txUsuario;
	private JLabel lbTelefono;
	private JLabel lbUsuario;
	private JPasswordField psRegistro;
	private JLabel lbContraseña;
	private JLabel lbConfirmacion;
	private JPasswordField psConfirmacion;
	private JCheckBox chMinorista;
	private JLabel lbInfo;
	private JLabel lbCuenta;
	private JTextField txNCuenta;
	private JLabel lbTarjeta;
	private JTextField txTarjeta;

	public VentanaRegistro(VentanaPrincipal vp) 
	{
		setResizable(false);
		setTitle("Tienda Electr\u00F3nica: Registro de usuario");
		this.vp = vp;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 657, 564);
		pnContenido = new JPanel();
		pnContenido.setBackground(Color.WHITE);
		pnContenido.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnContenido.setLayout(new BorderLayout(0, 0));
		setContentPane(pnContenido);
		pnContenido.add(getPanel(), BorderLayout.SOUTH);
		pnContenido.add(getPnCarrito(), BorderLayout.CENTER);
		
		setModal(true);
		setLocationRelativeTo(this);
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBackground(Color.WHITE);
			panel.add(getBtCerrar());
			panel.add(getBtRegistrarse());
		}
		return panel;
	}
	private JPanel getPnCarrito() {
		if (pnCarrito == null) {
			pnCarrito = new JPanel();
			pnCarrito.setBackground(Color.WHITE);
			pnCarrito.setLayout(null);
			pnCarrito.add(getLbTitulo());
			pnCarrito.add(getLbNombre());
			pnCarrito.add(getTextField_1());
			pnCarrito.add(getLbDNI());
			pnCarrito.add(getTextField_2());
			pnCarrito.add(getLbDireccion());
			pnCarrito.add(getTextField_3());
			pnCarrito.add(getTextField_1_1());
			pnCarrito.add(getTextField_2_1());
			pnCarrito.add(getLbTelefono());
			pnCarrito.add(getLbUsuario());
			pnCarrito.add(getPsRegistro());
			pnCarrito.add(getLbContraseña());
			pnCarrito.add(getLbConfirmacion());
			pnCarrito.add(getPsConfirmacion());
			pnCarrito.add(getChMinorista());
			pnCarrito.add(getLbInfo());
			pnCarrito.add(getLbCuenta());
			pnCarrito.add(getTxNCuenta());
			pnCarrito.add(getLbTarjeta());
			pnCarrito.add(getTxTarjeta());
		}
		return pnCarrito;
	}
	private JButton getBtCerrar() {
		if (btCerrar == null) {
			btCerrar = new JButton("Cerrar");
			btCerrar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					txNombre.setText("");
					txDNI.setText("");
					txDireccion.setText("");
					txTelefono.setText("");
					txUsuario.setText("");
					psRegistro.setText("");
					psConfirmacion.setText("");
					dispose();
				}
			});
			btCerrar.setMnemonic('C');
		}
		return btCerrar;
	}
	private JButton getBtRegistrarse() {
		if (btRegistrarse == null) {
			btRegistrarse = new JButton("Registrarse");
			btRegistrarse.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					if(confirmarDatos())
					{
						if(comprobarContraseñas())
						{
							char[] contraseñaCodex = psRegistro.getPassword();
							
							String contraseña = "";
							
							for(int i = 0; i < contraseñaCodex.length; i ++)
							{
								contraseña = contraseña + contraseñaCodex[i];
							}
							
							try 
							{
								vp.devolverGestor().registrarCliente(txUsuario.getText(), contraseña, txNombre.getText(), txDNI.getText(), txDireccion.getText(), txTelefono.getText(), 
										txNCuenta.getText(), txTarjeta.getText(), chMinorista.isSelected());
							} catch (SQLException e1) 
							{
								e1.printStackTrace();
							}
							
							mostrarExitoRegistro();
							
							dispose();
							
						}
						else
						{
							mostrarErrorContraseñas();
						}
					}
					else
					{
						mostrarError();
					}
				}
			});
			btRegistrarse.setMnemonic('R');
		}
		return btRegistrarse;
	}
	
	private boolean comprobarContraseñas()
	{
		char[] contraseñaCodex1 = psRegistro.getPassword();
		
		String contraseña1 = "";
		
		for(int i = 0; i < contraseñaCodex1.length; i ++)
		{
			contraseña1 = contraseña1 + contraseñaCodex1[i];
		}
		
		char[] contraseñaCodex2 = psConfirmacion.getPassword();
		
		String contraseña2 = "";
		
		for(int i = 0; i < contraseñaCodex2.length; i ++)
		{
			contraseña2 = contraseña2 + contraseñaCodex2[i];
		}
		
		if(contraseña1.equals(contraseña2))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private boolean confirmarDatos()
	{
		char[] contraseñaCodex1 = psRegistro.getPassword();
		
		String contraseña1 = "";
		
		for(int i = 0; i < contraseñaCodex1.length; i ++)
		{
			contraseña1 = contraseña1 + contraseñaCodex1[i];
		}
		
		char[] contraseñaCodex2 = psConfirmacion.getPassword();
		
		String contraseña2 = "";
		
		for(int i = 0; i < contraseñaCodex2.length; i ++)
		{
			contraseña2 = contraseña2 + contraseñaCodex2[i];
		}
		
		if(txNombre.getText().equals("") || txNombre.getText().equals(" ") || txDNI.getText().equals("") || txDNI.getText().equals(" ") || txDireccion.getText().equals("") || txDireccion.getText().equals(" ") 
				|| txTelefono.getText().equals("") || txTelefono.getText().equals(" ") || txUsuario.getText().equals("") || txUsuario.getText().equals(" ") || contraseña1.equals("")
				|| contraseña1.equals(" ") || contraseña2.equals("") || contraseña2.equals(" "))
		{
			return false;
		}
		else if((txNCuenta.getText().equals("") || txNCuenta.getText().equals(" ")) && (txTarjeta.getText().equals("") || txTarjeta.getText().equals(" ")))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private void mostrarError()
	{
		JOptionPane.showMessageDialog(this, "Debe de rellenar los campos correctamente, sin dejar huecos en blanco y sin carácteres incorrectos", "Error de registro", 2);
	}
	
	private void mostrarErrorContraseñas()
	{
		JOptionPane.showMessageDialog(this, "Las contraseñas deben de coincidir", "Error de registro", 2);
	}

	private void mostrarExitoRegistro()
	{
		JOptionPane.showMessageDialog(null, "!!!Registro completado con éxito!!!", "Información de registro", 1);
	}
	
	private JLabel getLbTitulo() {
		if (lbTitulo == null) {
			lbTitulo = new JLabel("Reg\u00EDstrate en la aplicaci\u00F3n rellenando este sencillo formulario:");
			lbTitulo.setFont(new Font("Tahoma", Font.PLAIN, 20));
			lbTitulo.setBounds(42, 11, 564, 59);
		}
		return lbTitulo;
	}
	private JLabel getLbNombre() {
		if (lbNombre == null) {
			lbNombre = new JLabel("Nombre:");
			lbNombre.setDisplayedMnemonic('N');
			lbNombre.setLabelFor(getTextField_1());
			lbNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbNombre.setBounds(181, 191, 84, 25);
		}
		return lbNombre;
	}
	private JTextField getTextField_1() {
		if (txNombre == null) {
			txNombre = new JTextField();
			txNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txNombre.setBounds(319, 191, 166, 25);
			txNombre.setColumns(15);
		}
		return txNombre;
	}
	private JLabel getLbDNI() {
		if (lbDNI == null) {
			lbDNI = new JLabel("DNI o NIF:");
			lbDNI.setLabelFor(getTextField_2());
			lbDNI.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbDNI.setDisplayedMnemonic('F');
			lbDNI.setBounds(181, 227, 84, 28);
		}
		return lbDNI;
	}
	private JTextField getTextField_2() {
		if (txDNI == null) {
			txDNI = new JTextField();
			txDNI.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) 
				{
					String dni = txDNI.getText();
					
					if(dni.length() != 9)
					{
						txDNI.setText("");
					}
					if(comprobarDNI() == false)
					{
						txDNI.setText("");
					}
				}
			});
			txDNI.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDNI.setColumns(15);
			txDNI.setBounds(319, 229, 166, 25);
		}
		return txDNI;
	}
	
	private boolean comprobarDNI()
	{
        char[] letraDni = {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D',  'X',  'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};  
  
        String num = "";
        int ind = 0;  
 
        String dni = txDNI.getText();
  
        if(dni.length() == 8) 
        {
             dni = "0" + dni;
        }
        if (!Character.isLetter(dni.charAt(8)))
        {
             return false;  
        }
        if (dni.length() != 9)
        {   
             return false;
        }  
        for (int i = 0; i < 8; i ++) 
        {
             if(!Character.isDigit(dni.charAt(i)))
             {
                   return false;    
             }

             num += dni.charAt(i);     
        }
  
        ind = Integer.parseInt(num);
  
        ind %= 23;
  
        if ((Character.toUpperCase(dni.charAt(8))) != letraDni[ind])
        {
             return false;
        }  
        
        return true; 
	}
	
	private JLabel getLbDireccion() {
		if (lbDireccion == null) {
			lbDireccion = new JLabel("Direcci\u00F3n:");
			lbDireccion.setLabelFor(getTextField_3());
			lbDireccion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbDireccion.setDisplayedMnemonic('D');
			lbDireccion.setBounds(181, 263, 84, 25);
		}
		return lbDireccion;
	}
	private JTextField getTextField_3() {
		if (txDireccion == null) {
			txDireccion = new JTextField();
			txDireccion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txDireccion.setColumns(15);
			txDireccion.setBounds(319, 263, 166, 25);
		}
		return txDireccion;
	}
	private JTextField getTextField_1_1() {
		if (txTelefono == null) {
			txTelefono = new JTextField();
			txTelefono.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) 
				{
					boolean error = false;
					try
					{
						Integer.parseInt(txTelefono.getText());
						
					}catch(NumberFormatException e)
					{
						error = true;
					}
					
					if(error || txTelefono.getText().length() != 9)
					{
						txTelefono.setText("");
					}
				}
			});
			txTelefono.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txTelefono.setColumns(15);
			txTelefono.setBounds(319, 299, 166, 25);
		}
		return txTelefono;
	}
	private JTextField getTextField_2_1() {
		if (txUsuario == null) {
			txUsuario = new JTextField();
			txUsuario.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) 
				{
					for(Cliente c: vp.devolverGestor().getArrayClientes())
					{
						String usuario = txUsuario.getText();
						
						if(c.getUsuario().equals(usuario))
						{
							txUsuario.setText("");
						}
					}
				}
			});
			txUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txUsuario.setColumns(15);
			txUsuario.setBounds(319, 81, 166, 25);
		}
		return txUsuario;
	}
	private JLabel getLbTelefono() {
		if (lbTelefono == null) {
			lbTelefono = new JLabel("Tel\u00E9fono:");
			lbTelefono.setLabelFor(getTextField_1_1());
			lbTelefono.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbTelefono.setDisplayedMnemonic('T');
			lbTelefono.setBounds(181, 299, 84, 26);
		}
		return lbTelefono;
	}
	private JLabel getLbUsuario() {
		if (lbUsuario == null) {
			lbUsuario = new JLabel("Usuario:");
			lbUsuario.setLabelFor(getTextField_2_1());
			lbUsuario.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbUsuario.setDisplayedMnemonic('U');
			lbUsuario.setBounds(181, 81, 84, 25);
		}
		return lbUsuario;
	}
	private JPasswordField getPsRegistro() {
		if (psRegistro == null) {
			psRegistro = new JPasswordField();
			psRegistro.setColumns(15);
			psRegistro.setFont(new Font("Tahoma", Font.PLAIN, 14));
			psRegistro.setBounds(319, 117, 166, 25);
		}
		return psRegistro;
	}
	private JLabel getLbContraseña() {
		if (lbContraseña == null) {
			lbContraseña = new JLabel("Contrase\u00F1a:");
			lbContraseña.setLabelFor(getPsRegistro());
			lbContraseña.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbContraseña.setDisplayedMnemonic('C');
			lbContraseña.setBounds(181, 118, 84, 25);
		}
		return lbContraseña;
	}
	private JLabel getLbConfirmacion() {
		if (lbConfirmacion == null) {
			lbConfirmacion = new JLabel("Repite Contrase\u00F1a:");
			lbConfirmacion.setLabelFor(getPsConfirmacion());
			lbConfirmacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbConfirmacion.setDisplayedMnemonic('R');
			lbConfirmacion.setBounds(181, 155, 128, 25);
		}
		return lbConfirmacion;
	}
	private JPasswordField getPsConfirmacion() {
		if (psConfirmacion == null) {
			psConfirmacion = new JPasswordField();
			psConfirmacion.setColumns(15);
			psConfirmacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
			psConfirmacion.setBounds(319, 155, 166, 25);
		}
		return psConfirmacion;
	}
	private JCheckBox getChMinorista() {
		if (chMinorista == null) {
			chMinorista = new JCheckBox("");
			chMinorista.setBackground(Color.WHITE);
			chMinorista.setBounds(181, 441, 21, 23);
		}
		return chMinorista;
	}
	private JLabel getLbInfo() {
		if (lbInfo == null) {
			lbInfo = new JLabel("Selecciona este cuadradito si eres minorista");
			lbInfo.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbInfo.setBounds(208, 430, 277, 34);
		}
		return lbInfo;
	}
	private JLabel getLbCuenta() {
		if (lbCuenta == null) {
			lbCuenta = new JLabel("N\u00BA de cuenta:");
			lbCuenta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbCuenta.setDisplayedMnemonic('T');
			lbCuenta.setBounds(181, 336, 128, 26);
		}
		return lbCuenta;
	}
	private JTextField getTxNCuenta() {
		if (txNCuenta == null) {
			txNCuenta = new JTextField();
			txNCuenta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent e) 
				{
					boolean error = false;
					
					try
					{
						Float.parseFloat(txNCuenta.getText());
						
					}catch(NumberFormatException e1)
					{
						error = true;
					}
					
					if(error || txNCuenta.getText().length() != 24)
					{
						txNCuenta.setText("");
					}
				}
			});
			txNCuenta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txNCuenta.setColumns(15);
			txNCuenta.setBounds(319, 335, 166, 25);
		}
		return txNCuenta;
	}
	private JLabel getLbTarjeta() {
		if (lbTarjeta == null) {
			lbTarjeta = new JLabel("N\u00BA de Tarjeta:");
			lbTarjeta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lbTarjeta.setDisplayedMnemonic('T');
			lbTarjeta.setBounds(181, 373, 128, 26);
		}
		return lbTarjeta;
	}
	private JTextField getTxTarjeta() {
		if (txTarjeta == null) {
			txTarjeta = new JTextField();
			txTarjeta.addFocusListener(new FocusAdapter() {
				@Override
				public void focusLost(FocusEvent arg0) 
				{
					boolean error = false;
					
					try
					{
						Float.parseFloat(txTarjeta.getText());
						
					}catch(NumberFormatException e)
					{
						error = true;
					}
					
					if(error || txTarjeta.getText().length() != 16)
					{
						txTarjeta.setText("");
					}
				}
			});
			txTarjeta.setFont(new Font("Tahoma", Font.PLAIN, 14));
			txTarjeta.setColumns(15);
			txTarjeta.setBounds(319, 371, 166, 25);
		}
		return txTarjeta;
	}
}